import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')

args = parser.parse_args()

repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR='/'.join(parts)
SOURCE_DIR = BASE_DIR + '/reposiotories/' + repo_name
print ('SOURCE_DIR', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print ('ANALYSIS_DIR', ANALYSIS_DIR)
