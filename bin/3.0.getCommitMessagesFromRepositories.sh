#!/bin/bash

export NO_PAGER="--no-pager"

export BASE_DIRECTORY=$(pwd)/..
export OLD_PWD=$(pwd)
export REPO_DIRECTORY="${BASE_DIRECTORY}/repositories"
export BIN_DIRECTORY="${BASE_DIRECTORY}/bin"
export ANALYSIS_DIRECTORY="${BASE_DIRECTORY}/analysis"

export LC_ALL=C

while [ $# -gt 0 ]; do
  case "$1" in
  -r | -repo | --repo)
    REPO="$2"
    ;;
  -a | -arg_1 | --arg_1)
    arg_1="$2"
    ;;
  *)
    printf "***************************\n"
    printf "* Error: Invalid argument.*\n"
    printf "***************************\n"
    exit 1
    ;;
  esac
  shift
  shift
done

export I=

cd $REPO_DIRECTORY

for i in $(ls -d */); do
  DIR=${i%%/}
  if [ "$DIR" == "$REPO" ]; then
    echo
    echo "==============================================================================="
    cd $DIR
    echo ">>>>> Processing repo $DIR"
    echo $(date)
    echo ">>>>> Description: Creates a messages.csv and stats.csv file by retrieving commit messages."
    echo ">>>>> Note: Run 'vim +\"set nobomb | set fenc=utf8 | x\" messages.csv' if the file can't be used in the next step."
    OUT_DIR="${BASE_DIRECTORY}/analysis/${DIR}"
    TMP_FILE="/tmp/${REPO}_messages.csv"
    OUT_FILE="${OUT_DIR}/messages.csv"

    mkdir -p $OUT_DIR
    rm -fr $OUT_DIR/*
    echo ">>>>> OUT_DIR: $OUT_DIR"
    echo ">>>>> OUT_FILE: ${OUT_FILE}"

    # Got warnings in linux repo
    git config diff.renameLimit 999999
    git config merge.renameLimit 999999

    echo "hash;parentHashes;author;authorDate;comitter;comitterDate;subject;comment" >$TMP_FILE
    git $NO_PAGER log --date=local --all --pretty="%H
%x3B%P
%x3B%an
%x3B%ad
%x3B%cn
%x3B%cd
%x3BCOMMENTMARK%sCOMMENTMARK
%x3BCOMMENTMARK%BCOMMENTMARK" | tr '"' "'" | tr "\r;" ';' >>$TMP_FILE
    perl -0777 -i -0pe 's/\n;/;/smg' $TMP_FILE
    #		sed $I '' 's/COMMENTMARK/\\\"/g' $TMP_FILE
    perl -0777 -i -0pe 's/COMMENTMARK/"/smg' $TMP_FILE

    mv $TMP_FILE $OUT_FILE

    echo "Finished file ${OUT_FILE}."
    echo $(date)

    #    OUT_FILE="${OUT_DIR}/stats.csv"
    #    TMP_FILE="/tmp/${REPO}_stats.csv"
    #
    #    echo "Starting processing $OUT_FILE"
    #    echo "hash;changedFiles;linesAdded;linesDeleted" >$TMP_FILE
    #    git $NO_PAGER log --date=local --all --pretty="NEWLINE%H%x3B" --shortstat | tr "\n" " " | tr "@" "\n" >>$TMP_FILE
    #
    #    perl -0777 -i -0pe 's/^NEWLINE//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/NEWLINE/\n/smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/, /;/smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/ files changed//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/ file changed//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/ insertions\(\+\)//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/ insertion\(\+\)//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/deletions\(-\)//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's/deletion\(-\)//smg' $TMP_FILE
    #    perl -0777 -i -0pe 's///smg' $TMP_FILE
    #    perl -0777  -C7 -i -0pe 's/;   /;/smg' $TMP_FILE
    #
    #    mv $TMP_FILE $OUT_FILE
    #
    #    echo "Finished file ${OUT_FILE}."
    #    echo `date`

    cd -
  fi
done

echo "Done with $REPO"
echo $(date)
