import os
import pandas as pd
import glob
import mysql.connector
import csv
from datetime import datetime

csv.field_size_limit(csv.field_size_limit() * 10)

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

BASE_DIRECTORY = os.getcwd()
WORKING_DIRECTORY = BASE_DIRECTORY + '/../analysis/'
os.chdir(WORKING_DIRECTORY)

repos = [f.name for f in os.scandir() if f.is_dir() and f.name != 'analysis' and f.name != '.ipynb_checkpoints']
repos.sort()

# repos = ['WordPress', 'angular', 'ansible', 'atom', 'azure-docs', 'bash', 'bitcoin', 'brew', 'cassandra', 'che',
# 'cpython', 'express', 'flutter', 'git', 'jenkins', 'kubernetes', 'latex2e', 'linux', 'mediawiki', 'nginx',
# 'notebook', 'rails', 'react-native', 'tensorflow', 'vscode']


repos = [
   'chromium', 'sonarqube', 'chaosmonkey', 'react'
]


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches',
    )
    return mydb


def get_repos_id(mydb, name):
    mycursor = mydb.cursor()
    query = 'select id from repos where name=%s'
    data = (name,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    repos_id = myresult[0][0]
    mycursor.close()
    return repos_id


mydb = create_db_connection()

for d in repos:

    path = r'' + WORKING_DIRECTORY + d + '/'
    all_files = glob.glob(path + "/*.csv")

    repos_id = get_repos_id(mydb, d)
    print("\nRepository " + path)
    print('repos_id', repos_id)

    mycursor = mydb.cursor()
    mycursor.execute("SET NAMES 'utf8mb4'")
    mycursor.execute("SET CHARACTER SET utf8mb4")
    mycursor.execute('SET character_set_connection=utf8mb4')

    for filename in all_files:
        if 'merged.csv' in filename:
            print(">>>>> " + filename)
            df = pd.read_csv(filename, delimiter=';', quotechar='"', header=0,
                             engine='python')  # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False
            print(df.shape)

            for index, row in df.iterrows():
                hashes = row[0] if row[0] == row[0] else ''
                parentHashes = row[1] if row[1] == row[1] else ''
                author = row[2] if row[2] == row[2] else ''
                authorDate = row[3] if row[3] == row[3] else ''
                comitter = row[4] if row[4] == row[4] else ''
                comitterDate = row[5] if row[5] == row[5] else ''
                subject = row[6] if row[6] == row[6] else ''
                comment = row[7] if row[7] == row[7] else ''
                changedFiles = row[8] if row[8] == row[8] else 0
                linesAdded = row[9] if row[9] == row[9] else 0
                linesDeleted = row[10] if row[10] == row[10] else 0

                authorDate = datetime.strptime(authorDate, '%a %b %d %H:%M:%S %Y')
                comitterDate = datetime.strptime(comitterDate, '%a %b %d %H:%M:%S %Y')

                # 
                query = 'select id from repos_meta where repos_id=%s and hash=%s'
                data = (repos_id, hashes,)
                mycursor.execute(query, data)
                myresult = mycursor.fetchall()
                count = mycursor.rowcount
                meta_repos_id = 0

                if count == 0:
                    query = "insert into repos_meta (repos_id, hash, parentHashes, author, authorDate, comitter, comitterDate, subject, comment, changedFiles, linesAdded, linesDeleted) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                    data = (
                        repos_id, hashes, parentHashes, author, authorDate, comitter, comitterDate, subject, comment,
                        changedFiles, linesAdded, linesDeleted,)
                else:
                    meta_repos_id = myresult[0][0]
                    query = "update repos_meta set "
                    query += "repos_id=%s,"
                    query += "hash=%s,"
                    query += "parentHashes=%s,"
                    query += "author=%s,"
                    query += "authorDate=%s,"
                    query += "comitter=%s,"
                    query += "comitterDate=%s,"
                    query += "subject=%s,"
                    query += "comment=%s,"
                    query += "changedFiles=%s,"
                    query += "linesAdded=%s,"
                    query += "linesDeleted=%s,"
                    query += "modification_date=now(),"
                    query += "attempts=attempts+1 "
                    query += "where id=%s"
                    data = (
                        repos_id, hashes, parentHashes, author, authorDate, comitter, comitterDate, subject, comment,
                        changedFiles, linesAdded, linesDeleted, meta_repos_id,)
                try:
                    mycursor.execute(query, data)
                    mydb.commit()
                    meta_repos_id = mycursor.lastrowid
                except mysql.connector.Error as err:
                    print("Something went wrong: {}".format(err))
                    print('meta_repos_id: ', meta_repos_id, query, data)

    mycursor.close()

mydb.close()
