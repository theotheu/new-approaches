# !pip install mysql - connector

import nltk
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()
import mysql.connector
import re

words = [
    'abstractions',
    'Accessibility',
    'Accountability',
    'Adaptability',
    'Analyzability',
    'anti-fragility',
    'approaches',
    'Appropriateness recognizability',
    'architecture descriptions',
    'architecture frameworks',
    'architecture models',
    'architecture styles',
    'architecture viewpoints',
    'architecture views',
    'architectures',
    'arguments',
    'assumptions',
    'Authenticity',
    'Availability',
    'axiomas',
    'because',
    'behalf of',
    'bugs',
    'Capacity',
    'caused',
    'causes',
    'clarifications',
    'clarifies',
    'client-server'
    'Co-existence',
    'Compatibility',
    'Compliance',
    'components',
    'concerns',
    'Confidentiality',
    'connectors',
    'connections',
    'considering',
    'constraints',
    'corrections',
    'corrective',
    'correspondence rules',
    'definitions',
    'deployments',
    'descriptions',
    'descriptions',
    'designs',
    'discussions',
    'docs',
    'documentation',
    'documenting',
    'due to',
    'duplicate code',
    'empirical',
    'encapsulations',
    'ethics',
    'experiments',
    'explains',
    'explanations',
    'extends',
    'extensibility',
    'Fault tolerance',
    'features',
    'frameworks',
    'fucked',
    'Functional appropriateness',
    'Functional completeness',
    'Functional correctness',
    'Functional requirements',
    'Functional Suitability',
    'fundamentals',
    'governance',
    'how',
    'hypothesis',
    'iec',
    'ieee',
    'improved',
    'improvements',
    'inferences',
    'Installability',
    'Integrity',
    'interests',
    'interfaces',
    'Interoperability',
    'iso',
    'layering',
    'layers',
    'Learnability',
    'lose couplings'
    'low couplings',
    'Maintainability',
    'Maturity',
    'mean time before failure',
    'mean time to acknowledge',
    'mean time to failure',
    'mean time to recovery',
    'mean time to resolve',
    'mean time to respond',
    'model kind',
    'Modifiability',
    'modules',
    'Modularity',
    'MTBF',
    'MTTA',
    'MTTF',
    'MTTR',
    'new components',
    'new functions',
    'new methods',
    'new ui',
    'non-Functional',
    'non-functional',
    'Non-repudiation',
    'now that',
    'observations',
    'Operability',
    'packages',
    'paradigmata',
    'paradigms',
    'perfective',
    'Performance efficiency',
    'Portability',
    'pipe-and-filter',
    'postulates',
    'premises',
    'principles',
    'problems',
    'Project Budgets',
    'Project Timings',
    'propositions',
    'publish-subscribe',
    'pub-sub',
    'rationale',
    'reasons',
    'Recoverability',
    'redesign',
    'refactoring',
    'regulations',
    'related',
    'relations',
    'Reliability',
    'remove duplicates',
    'Replaceability',
    'requirements',
    'resilience',
    'Resource utilization',
    'results',
    'Reusability',
    'rework',
    'rfc',
    'rules',
    'sake of',
    'secure',
    'Security',
    'separation of concerns',
    'since',
    'solutions',
    'specifications',
    'stakeholders',
    'standards',
    'structures',
    'subprograms',
    'subsystems',
    'Support',
    'system-of-interest',
    'systems',
    'tasks',
    'Testability',
    'theoretical',
    'theories',
    'Time behavior',
    'Trainings',
    'variability',
    'Usability',
    'User error protection',
    'User interface aesthetics',
    'was not',
    'were not',
    'what',
    'why',
]


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    return mydb


def cleaner_stemmer_lemmatizer(field):
    # https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-software-engineering-use-case-779d4a28ba5

    field = field.lower()
    words_only = []
    stemmed = []
    lemmatized = []

    for w in re.sub("[^a-zA-Z]", " ", field).split():
        sw = stemmer.stem(w)
        lw = wordnet_lemmatizer.lemmatize(w)
        words_only.append(w)
        stemmed.append(sw)
        lemmatized.append(lw)

    separator = ' '
    words_only = separator.join(words_only)
    stemmed = separator.join(stemmed)
    lemmatized = separator.join(lemmatized)

    retVal = [words_only, stemmed, lemmatized]
    #     print ('=================================== original', field)
    #     print ('=================================== words_olny', words_olny)
    #     print ('=================================== stemmed', stemmed)
    #     print ('=================================== lemmatized', lemmatized)

    return retVal


mydb = create_db_connection()
mycursor = mydb.cursor(buffered=True)

for w in words:
    # w = w.lower()
    # w = re.sub("[^a-zA-Z]", " ", w).split()
    # s = stemmer.stem(w)
    # l = wordnet_lemmatizer.lemmatize(w)
    # print(w, '|||', s, '|||', l)
    r = cleaner_stemmer_lemmatizer(w)
    p = r[0]
    s = r[1]
    l = r[2]
    print(w, p, s, l)

    query = 'select id from search_terms where pattern=%s'
    data = (p,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    count = mycursor.rowcount

    if count == 0:
        query = 'insert into search_terms (pattern, stemmed, lemmatized, description, is_active) values '
        query += '(%s, %s, %s, %s, 0)'
        data = (p, s, l, w)
        try:
            mycursor.execute(query, data)
            mydb.commit()
            search_results_id = mycursor.lastrowid
        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))
            print('search_results_id: ', search_results_id, query, data)

mycursor.close()
mydb.close()
