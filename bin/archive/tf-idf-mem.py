import mysql.connector
import re
import nltk
import math
import gc
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from datetime import datetime

nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()


start_time0 = datetime.now()

limit = 5000000
cache_hits = 0
cache_misses = 0
document_counter = 0
unique_word_counter = 0
string_length_treshold = 2
stop_words = set(stopwords.words('english'))

plain_words_dict = {}
plain_unique_words_dict = {}
stemmed_unique_words_dict = {}
stemmed_words_dict = {}
wl_dict = {}
words = {}
total_number_of_plain_words_per_comment = {}
plain_stats_dict = {}
total_number_of_stemmed_words_per_comment = {}


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    return mydb


def save_term_tf(repos_meta_id, w, s):
    global unique_word_counter
    #####
    if w not in plain_words_dict:
        plain_words_dict[w] = {}
        plain_unique_words_dict[w] = 0
        unique_word_counter += 1

    repos_meta_id = str(repos_meta_id)
    if repos_meta_id not in plain_words_dict[w]:
        plain_words_dict[w][repos_meta_id] = {}

    if 'bow' not in plain_words_dict[w][repos_meta_id]:
        plain_words_dict[w][repos_meta_id]['bow'] = 0
        plain_words_dict[w][repos_meta_id]['tf'] = 0
        plain_words_dict[w][repos_meta_id]['idf'] = 0
        plain_words_dict[w][repos_meta_id]['tfidf'] = 0

    plain_words_dict[w][repos_meta_id]['bow'] += 1
    plain_unique_words_dict[w] += 1

    ####### STEMMED
    if s not in stemmed_words_dict:
        stemmed_words_dict[s] = {}
        stemmed_unique_words_dict[s] = 0

    repos_meta_id = str(repos_meta_id)
    if repos_meta_id not in stemmed_words_dict[s]:
        stemmed_words_dict[s][repos_meta_id] = {}

    if 'bow' not in stemmed_words_dict[s][repos_meta_id]:
        stemmed_words_dict[s][repos_meta_id]['bow'] = 0
        stemmed_words_dict[s][repos_meta_id]['tf'] = 0
        stemmed_words_dict[s][repos_meta_id]['idf'] = 0
        stemmed_words_dict[s][repos_meta_id]['tfidf'] = 0

    stemmed_words_dict[s][repos_meta_id]['bow'] += 1
    stemmed_unique_words_dict[s] += 1

def parse_record(repos_meta_id, document):
    g = gc.collect()
    if (repos_meta_id % 1000 == 0):
    	print ("parsing document", repos_meta_id, datetime.now())
    for w in re.sub("[^a-zA-Z]", " ", document.lower()).split():
        # for w in re.sub("[\!\(\)\-\[\]\{\}\;\?\@\#\$%\:\'\"\\,\.\/^\&\*\_]", " ", document.lower()).split():
        if not w in stop_words and len(w) > string_length_treshold:
            s = stemmer.stem(w)
            l = wordnet_lemmatizer.lemmatize(w)
            words[w] = [s, l]
            save_term_tf(repos_meta_id, w, s)


def _get_records(mycursor):
    myresult = {}
    global document_counter

    # https://dataaspirant.com/tf-idf-term-frequency-inverse-document-frequency/
    myresult['0'] = 'i read the svm algorithm article in dataaspriant blog'
    myresult['1'] = 'i read the randomforest algorithm article in dataaspriant blog'

    # https://sit.instructure.com/courses/1580/pages/tf-idf-excel-example-file
    # myresult['c1'] = 'Human machine interface for ABC computer application.'
    # myresult['c2'] = 'A survey of user opinion of computer system response time.'
    # myresult['c3'] = 'The EPS user interface management system.'
    # myresult['c4'] = 'System and human system engineering testing in EPS.'
    # myresult['c5'] = 'Relation to user perceived response time to error measurement.'
    # myresult['m1'] = 'The generation of random, binary, ordered trees.'
    # myresult['m2'] = 'The intersection graph of paths in trees.'
    # myresult['m3'] = 'Graph minors IV: Widths of trees and well-quasi-ordering.'
    # myresult['m4'] = 'Graph minors: A survey.'

    # https://towardsdatascience.com/a-gentle-introduction-to-calculating-the-tf-idf-values-9e391f8a13e5
    # myresult['c1'] = 'you were born with potential'
    # myresult['c2'] = 'you were born with goodness and trust'
    # myresult['c3'] = 'you were born with ideals and dreams'
    # myresult['c4'] = 'you were born with greatness'
    # myresult['c5'] = 'you were born with wings'
    # myresult['m2'] = "you are not meant for crawling, so don't"
    # myresult['m3'] = 'you have wings'
    # myresult['m4'] = 'learn to use them and fly'

    # https://community.rapidminer.com/discussion/46333/term-frequencies-and-tf-idf-how-are-these-calculated
    # myresult['a'] = 'report data mining text mining'
    # myresult['b'] = 'text video analysis'

    # https: // iksinc.online / category / nlp /
    # myresult['a'] = 'The sky is blue and beautiful'
    # myresult['b'] = 'The king is old and the queen is beautiful'
    # myresult['c'] = 'Love this beautiful blue sky'
    # myresult['d'] = 'The beautiful queen and the old king'

    # https://towardsdatascience.com/getting-started-with-text-vectorization-2f2efbec6685
    # myresult['a'] = "This is a brown house. This house is big. The street number is 1."
    # myresult['b'] = "This is a small house. This house has 1 bedroom. The street number is 12."
    # myresult['c'] = "This dog is brown. This dog likes to play."
    # myresult['d'] = "The dog is in the bedroom."

    for repos_meta_id in myresult:
        document_counter = document_counter + 1
        parse_record(repos_meta_id, myresult[repos_meta_id])
    print("################################ record", document_counter)


def get_records(mycursor):
    global document_counter
    query = 'select id, comment from repos_meta where tf_attempts=0 limit %s'
    data = (limit,)
    # data = ()
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    # document_counter = 0
    print('get_records: ', datetime.now() - start_time0)
    for repos_meta_id, document in myresult:
        document_counter = document_counter + 1
        parse_record(repos_meta_id, document)
    print("################################ record", document_counter)


def export_words_stemmed_lemmatized():
    f = open("_words_stemmed_lemmatized.csv", "w")
    for w in words:
        l = '\t'.join([w, words[w][0], words[w][1]])
        l += '\n'
        f.write(l)
    f.close()


def export_plain_words_with_repos_meta_id():
    f = open("_plain_words_with_repos_meta_id.csv", "w")
    for w in plain_words_dict:
        for meta_repos_id in plain_words_dict[w]:
            bow = str(plain_words_dict[w][meta_repos_id]['bow'])
            tf = str(plain_words_dict[w][meta_repos_id]['tf'])
            tfidf = str(plain_words_dict[w][meta_repos_id]['tfidf'])
            l = '\t'.join([w, str(meta_repos_id), bow, tf, tfidf])
            l += '\n'
            f.write(l)
    f.close()


def export_stemmed_words_with_repos_meta_id():
    f = open("_stemmed_words_with_repos_meta_id.csv", "w")
    for w in stemmed_words_dict:
        for meta_repos_id in stemmed_words_dict[w]:
            l = '\t'.join([w, str(meta_repos_id), str(stemmed_words_dict[w][meta_repos_id])])
            l += '\n'
            f.write(l)
    f.close()


def count_words_per_lemma():
    total_number_of_plain_words = 0
    total_number_of_stemmed_words = 0
    total_number_of_unique_plain_words = 0
    total_number_of_unique_stemmed_words = 0

    for w in plain_words_dict:
        total_number_of_unique_plain_words += 1
        for repos_meta_id in plain_words_dict[w]:
            number_of_words = plain_words_dict[w][repos_meta_id]['bow']
            if repos_meta_id not in total_number_of_plain_words_per_comment:
                total_number_of_plain_words_per_comment[repos_meta_id] = 0
            total_number_of_plain_words_per_comment[repos_meta_id] += number_of_words
            total_number_of_plain_words += number_of_words
    #######
    for w in stemmed_words_dict:
        total_number_of_unique_stemmed_words += 1
        for repos_meta_id in stemmed_words_dict[w]:
            number_of_words = stemmed_words_dict[w][repos_meta_id]['bow']
            if repos_meta_id not in total_number_of_stemmed_words_per_comment:
                total_number_of_stemmed_words_per_comment[repos_meta_id] = 0
            total_number_of_stemmed_words_per_comment[repos_meta_id] += number_of_words
            total_number_of_stemmed_words += number_of_words

    l = [total_number_of_plain_words,
         total_number_of_stemmed_words,
         total_number_of_unique_plain_words,
         total_number_of_unique_stemmed_words]

    return l


def do_statistics(l):
    global document_counter
    global unique_word_counter
    print("Number of documents in corpus: ", document_counter)
    print("Unique plain words in corpus : ", unique_word_counter)
    print("Unique words : ", plain_unique_words_dict)
    total_number_of_plain_words = l[0]
    total_number_of_stemmed_words = l[1]
    total_number_of_unique_plain_words = l[2]
    total_number_of_unique_stemmed_words = l[3]

    print(total_number_of_plain_words, total_number_of_stemmed_words, total_number_of_unique_plain_words,
          total_number_of_unique_stemmed_words)

    for w in plain_words_dict:
        unique_words_in_corpus = plain_unique_words_dict[w]
        number_of_documents_in_corpus_with_term = 0
        print('WORD: ', w)
        for repos_meta_id in plain_words_dict[w]:
            number_of_documents_in_corpus_with_term = len(plain_words_dict[w])
            print('number_of_documents_in_corpus_with_term: ', number_of_documents_in_corpus_with_term)

            l = total_number_of_plain_words_per_comment[repos_meta_id]
            print('total_number_of_plain_words_per_comment: ', l)
            bow = plain_words_dict[w][repos_meta_id]['bow']
            tf = bow / l
            idf = math.log10(document_counter / number_of_documents_in_corpus_with_term)
            print('bow:', bow, ', tf:', tf, ', idf: ', idf, ', tf*idf: ', tf * idf)

            tfidf = tf * idf
            tfidf_smooth = tf * idf

            plain_words_dict[w][repos_meta_id]['tf'] = tf
            plain_words_dict[w][repos_meta_id]['idf'] = idf
            plain_words_dict[w][repos_meta_id]['tfidf'] = tfidf



mydb = create_db_connection()
mycursor = mydb.cursor(buffered=True)
get_records(mycursor)
mycursor.close()
mydb.close()

l = count_words_per_lemma()
do_statistics(l)

export_words_stemmed_lemmatized()
export_plain_words_with_repos_meta_id()
export_stemmed_words_with_repos_meta_id()

print('TOTAL DURATION for ', limit, 'records ', datetime.now() - start_time0)
