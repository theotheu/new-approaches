import sys
import csv
import pandas as pd
import nltk
from nltk import word_tokenize
from datetime import datetime
import re
from nltk.corpus import stopwords

nltk.download('punkt')
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()
import mysql.connector
import os, psutil, gc

# mydb = mysql.connector.connect(
#     host="node157.tezzt.nl",
#     user="theotheu",
#     password='aQ743Gp-',
#     database='new_approaches'
# )

csv.field_size_limit(100000000)

# from nltk.corpus import stopwords
# words = stopwords.words("english")
stopwords = []
commits = 0

start_time0 = datetime.now()


def save_search_terms_to_database(df):
    for index, row in df.iterrows():
        pattern = row.pattern
        stemmed = row.stemmed
        lemmatized = row.lemmatized

        mydb = mysql.connector.connect(
            host="node157.tezzt.nl",
            user="theotheu",
            password='aQ743Gp-',
            database='new_approaches'
        )

        mycursor = mydb.cursor()
        query = "select id from search_terms where pattern = %s"
        data = (pattern,)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount

        if (count == 0):
            query = 'insert into search_terms (pattern, stemmed, lemmatized) values (%s, %s, %s)'
            data = (pattern, stemmed, lemmatized)
            mycursor.execute(query, data)
            mydb.commit()

        mydb.close()


def import_search_termes():
    start_time = datetime.now()

    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    print("Start importin search patterns...")
    filename = './search_patterns.csv'
    df_search_terms = pd.read_csv(filename, delimiter=';', quotechar='"',
                                  engine='python')  # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False

    mydb.close()

    print('Duration: ', datetime.now() - start_time, "seconds")
    return df_search_terms


def stemming_and_lemmatizing_search_words(df_search_terms):
    start_time = datetime.now()
    print("Start stemming and lemmatizing search patterns...")
    stemmer = PorterStemmer()
    wordnet_lemmatizer = WordNetLemmatizer()

    pattern = df_search_terms.pattern
    df_search_terms['stemmed'] = pattern.apply(
        lambda x: " ".join(
            [stemmer.stem(i) for i in re.sub("[^a-zA-Z]", " ", x).split() if i not in stopwords]).lower())
    df_search_terms['lemmatized'] = pattern.apply(lambda x: " ".join(
        [wordnet_lemmatizer.lemmatize(i) for i in re.sub("[^a-zA-Z]", " ", x).split() if i not in stopwords]).lower())

    print('Duration: ', datetime.now() - start_time, "seconds")
    return df_search_terms


def import_repo(repo):
    start_time = datetime.now()
    print("Start importing repo...")

    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )

    filename = '../analysis/' + repo + '/merged.csv'
    # filename = '../analysis/linux/merged.csv'

    df_repo = pd.read_csv(filename, delimiter=';', quotechar='"', header=0,
                          engine='python')  # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False
    commits = df_repo.shape[0]

    query = 'update repos set commits = ' + str(commits) + ', modification_date=now() where name=\'' + repo + '\''
    mycursor = mydb.cursor()
    mycursor.execute(query)
    mydb.commit()


    mydb.close()

    print('Duration: ', datetime.now() - start_time, "seconds")

    return df_repo


def stemming_and_lemmatizing_repo(df_repo):
    start_time = datetime.now()
    print("Start stemming, lemmatizing repo...")

    s = []
    l = []

    for index, row in df_repo.iterrows():
        g = gc.collect()
        print('processing row', index, g)
        m = psutil.virtual_memory()
        print ('             ', m)
        if row.comment != row.comment:
            row.comment = ''
        stemmed = row.comment.join(
            [stemmer.stem(i) for i in str(re.sub("[^a-zA-Z]", " ", row.comment)).split()]).lower()
        lemmatized = row.comment.join(
            [wordnet_lemmatizer.lemmatize(i) for i in str(re.sub("[^a-zA-Z]", " ", row.comment)).split() if
             i not in stopwords]).lower()
        s.append(stemmed)
        l.append(lemmatized)

    df_repo['stemmed'] = s
    df_repo['lemmatized'] = l
    # print (df_repo)

    print('Duration: ', datetime.now() - start_time, "seconds")
    return df_repo


def creating_dictionary(df_search_terms):
    start_time = datetime.now()
    print("Start creating dictionary...")

    search_dictionary = {}
    for index, row in df_search_terms.iterrows():
        _r = {}
        _r['pattern'] = {'k': row.pattern, 'v': 0}
        _r['stemmed'] = {'k': row.stemmed, 'v': 0}
        _r['lemmatized'] = {'k': row.lemmatized, 'v': 0}
        search_dictionary[row.pattern] = _r

    print('Duration: ', datetime.now() - start_time, "seconds")
    return search_dictionary

    mydb.close()


def searching(df_search_terms, df_repo, search_dictionary):
    start_time = datetime.now()
    print("Start searching...")

    for index1, row1 in df_search_terms.iterrows():
        g = gc.collect()
        print('processing row', index, g)
        m = psutil.virtual_memory()
        print ('     ', m)
        for index2, row2 in df_repo.iterrows():

            p_s = row1.pattern
            s_s = row1.stemmed
            l_s = row1.lemmatized

            c = row2.comment
            if c != c:
                c = ''

            try:
                f = len(re.findall(p_s, c))
            except:
                print(p_s, c)

            search_dictionary[p_s]['pattern']['v'] = search_dictionary[p_s]['pattern']['v'] + f

    print('Duration: ', datetime.now() - start_time, "seconds")

    return search_dictionary


def save_results(repo, search_dictionary):
    start_time = datetime.now()

    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )

    mycursor = mydb.cursor()

    # Get repos_id
    query = "select id from repos where name = %s"
    data = (repo,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()

    repos_id = myresult[0][0]

    header = ["pattern", "found", "stemmed", "found", "lemmatized", "found"]
    with open('../analysis/' + repo + '/data.csv', 'w', encoding='utf-8-sig') as f:
        writer = csv.writer(f, delimiter=';', quotechar='"')
        writer.writerow(header)  # write the header
        l = []
        for key in search_dictionary:
            if (len(l) > 0):
                pattern = l[0]
                patterns_found = l[1]
                stemmed_found = l[3]
                lemmatized_found = l[5]
                writer.writerow(l)
                # Get search_terms_id
                query = "select id from search_terms where pattern = %s"
                data = (pattern,)
                mycursor.execute(query, data)
                myresult = mycursor.fetchall()
                try:
                    search_term_id = myresult[0][0]
                except:
                    print('ERROR', pattern, query)

                # Check if result is defined
                query = "select id from search_results where repos_id = %s and search_terms_id = %s"
                data = (repos_id, search_term_id,)

                mycursor.execute(query, data)
                myresult = mycursor.fetchall()
                count = mycursor.rowcount
                if count == 0:
                    # Create search result record
                    query = "insert into search_results (repos_id, search_terms_id) values (%s, %s)"
                    data = (repos_id, search_term_id,)
                    mycursor.execute(query, data)
                    mydb.commit()
                    search_result_id = mycursor.lastrowid
                else:
                    search_result_id = myresult[0][0]

                query = 'update search_results set patterns_found=%s, stemmed_found=%s, lemmatized_found=%s, modification_date=now() where repos_id=%s and search_terms_id=%s'
                data = (patterns_found, stemmed_found, lemmatized_found, repos_id, search_term_id,)

                mycursor.execute(query, data)
                mydb.commit()

            n = search_dictionary[key]
            l = [];
            for k in n:
                column_name = n[k]['k']
                column_value = n[k]['v']
                l.append(column_name)
                l.append(column_value)

    print('Duration: ', datetime.now() - start_time, "seconds")
    d = datetime.now() - start_time0
    print('TOTAL EXECUTION TIME: ', d, "seconds")

    query = "update repos set searching_duration=%s, modification_date=now() where id=%s"
    data = (d, repos_id,)
    mycursor.execute(query, data)
    mydb.commit()

    mydb.close()


df_search_terms = import_search_termes()
stemming_and_lemmatizing_search_words(df_search_terms)
save_search_terms_to_database(df_search_terms)

repos = ['WordPress', 'angular', 'ansible', 'atom', 'azure-docs', 'bash', 'bitcoin', 'brew', 'cassandra', 'che',
         'cpython', 'express', 'flutter', 'git', 'jenkins', 'kubernetes', 'latex2e', 'linux', 'mediawiki', 'nginx',
         'notebook', 'rails', 'react-native', 'tensorflow', 'vscode']

repos = ['linux', 'mediawiki', 'nginx',
         'notebook', 'rails', 'react-native', 'tensorflow', 'vscode']

for repo in repos:
    print("======================================== REPO", repo)
    start_time0 = datetime.now()
    process = psutil.Process(os.getpid())
    print('memory used', process.memory_info().rss)

    ##### https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-software-engineering-use-case-779d4a28ba5

    df_repo = import_repo(repo)
    df_repo = stemming_and_lemmatizing_repo(df_repo)
    search_dictionary = creating_dictionary(df_search_terms)
    search_dictionary = searching(df_search_terms, df_repo, search_dictionary)
    save_results(repo, search_dictionary)
