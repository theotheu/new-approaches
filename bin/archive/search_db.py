# import
import mysql.connector
import re
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()
from nltk.tokenize import sent_tokenize, word_tokenize
from datetime import datetime
import sys

limit = 100


# create db
def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    return mydb


def get_repo_rows(mydb):
    mycursor = mydb.cursor(buffered=True)
    query = 'select id, repos_id, `comment` from repos_meta where attempts=0 limit %s'
    data = (limit,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    return myresult


def cleaner_stemmer_lemmatizer(field):
    # https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-software-engineering-use-case-779d4a28ba5

    field = field.lower()
    words = word_tokenize(field)
    words_only = []
    stemmed = []
    lemmatized = []

    for w in re.sub("[^a-zA-Z]", " ", field).split():
        sw = stemmer.stem(w)
        lw = wordnet_lemmatizer.lemmatize(w)
        words_only.append(w)
        stemmed.append(sw)
        lemmatized.append(lw)

    separator = ' '
    words_only = separator.join(words_only)
    stemmed = separator.join(stemmed)
    lemmatized = separator.join(lemmatized)

    retVal = [words_only, stemmed, lemmatized]
    #     print ('=================================== original', field)
    #     print ('=================================== words_olny', words_olny)
    #     print ('=================================== stemmed', stemmed)
    #     print ('=================================== lemmatized', lemmatized)

    return retVal


def read_search_terms(mydb):
    r = []
    mycursor = mydb.cursor(buffered=True)
    query = 'select * from search_terms order by pattern'
    mycursor.execute(query)
    myresult = mycursor.fetchall()
    mycursor.close()
    for search_term in myresult:
        search_terms_id = str(search_term[0])
        search_pattern = str(search_term[2], 'utf-8')
        search_stemmed = str(search_term[3], 'utf-8')
        search_lemmatized = str(search_term[4], 'utf-8')
        t = (search_terms_id, search_pattern, search_stemmed, search_lemmatized)
        r.append(t)
    return r


def save_to_commits_with_search_results(mydb, repos_meta_id, search_terms_id, patterns_found, stemmed_found,
                                        lemmatized_found):
    f = patterns_found + stemmed_found + lemmatized_found
    if f == 0:
        return

    mycursor = mydb.cursor(buffered=True)
    query = 'select id from commits_with_search_results where repos_meta_id=%s and search_terms_id=%s'
    data = (repos_meta_id, search_terms_id,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    count = mycursor.rowcount
    if count == 0:
        query = 'insert into commits_with_search_results (repos_meta_id, search_terms_id, updates) values '
        query += '(%s, %s, %s)'
        data = (repos_meta_id, search_terms_id, 1)
    else:
        commits_with_search_results_id = myresult[0][0]
        query = 'update commits_with_search_results set '
        query += 'updates=updates+1, '
        query += 'modification_date=now() where id = %s'
        data = (commits_with_search_results_id,)

    print('save_to_commits_with_search_results|', f, '|||', query, data)

    try:
        mycursor.execute(query, data)
        mydb.commit()
    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))
        print('query, data: ', query, data)

    mycursor.close()
    # sys.exit()


def save_search_result(mydb, repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found):

    save_to_commits_with_search_results(mydb, repos_meta_id, search_terms_id, patterns_found, stemmed_found,
                                        lemmatized_found)

    mycursor = mydb.cursor(buffered=True)
    query = 'select id from search_results where repos_id=%s and search_terms_id=%s'
    data = (repos_id, search_terms_id,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    count = mycursor.rowcount
    search_results_id=0

    if count == 0:
        query = 'insert into search_results (repos_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found) values '
        query += '(%s, %s, %s, %s, %s)'
        data = (repos_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found)
        print('save_search_result|', query, data)

        try:
            mycursor.execute(query, data)
            mydb.commit()
            search_results_id = mycursor.lastrowid
        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))
            print('search_results_id: ', search_results_id, query, data)
    else:
        if patterns_found + stemmed_found + lemmatized_found >0:
            search_results_id = myresult[0][0]
            query = 'update search_results set '
            query += 'patterns_found=patterns_found+%s, '
            query += 'stemmed_found=stemmed_found+%s, '
            query += 'lemmatized_found=lemmatized_found+%s, '
            query += 'modification_date=now(), '
            query += 'updates=updates+1 '
            query += 'where id=%s'
            data = (patterns_found, stemmed_found, lemmatized_found, search_results_id,)

            print('save_search_result|', query, data)

            try:
                mycursor.execute(query, data)
                mydb.commit()
                search_results_id = mycursor.lastrowid
            except mysql.connector.Error as err:
                print("Something went wrong: {}".format(err))
                print('search_results_id: ', search_results_id, query, data)


    mycursor.close()

# print('save_search_result', repos_meta_id, repos_id, search_terms_id, f_search_pattern, f_search_stemmed, f_search_lemmatized)


def update_repos_meta(my_db, repos_meta_id, search_terms_id):
    mycursor = mydb.cursor(buffered=True)
    query = 'update repos_meta set attempts=attempts+1, modification_date=now() where id=%s'
    data = (repos_meta_id,)
    print ('update_repos_meta|', query, data)
    mycursor.execute(query, data)
    mydb.commit()

    query = 'UPDATE repos SET commits = (SELECT COUNT(1) FROM repos_meta WHERE repos_id = %s) WHERE id = %s'
    data = (repos_id, repos_meta_id,)
    mycursor.execute(query, data)
    mydb.commit()

    mycursor.close()


start_time0 = datetime.now()
mydb = create_db_connection()

search_rows = read_search_terms(mydb)

repo_row_counter = 0
repo_rows = get_repo_rows(mydb)
for repo_row in repo_rows:
    repo_row_counter += 1
    start_time = datetime.now()
    repos_meta_id = repo_row[0]
    repos_id = repo_row[1]
    cleaned_stemmed_lemmatized = cleaner_stemmer_lemmatizer(repo_row[2])
    f = 0
    total_found = 0
    word_boundary = '\\b'

    for search_row in search_rows:
        search_terms_id = search_row[0]
        search_pattern = word_boundary + search_row[1] + word_boundary
        search_stemmed = word_boundary + search_row[2] + word_boundary
        search_lemmatized = word_boundary + search_row[3] + word_boundary

        f_search_pattern = len(re.findall(search_pattern, cleaned_stemmed_lemmatized[0]))
        f_search_stemmed = len(re.findall(search_stemmed, cleaned_stemmed_lemmatized[1]))
        f_search_lemmatized = len(re.findall(search_lemmatized, cleaned_stemmed_lemmatized[2]))


        if (search_pattern == search_stemmed and f_search_pattern != f_search_stemmed) or            (search_pattern == search_lemmatized and f_search_pattern != f_search_lemmatized) or   (search_pattern == search_lemmatized and f_search_pattern != f_search_lemmatized):
            print('!!!!!!!! search_pattern', search_pattern, f_search_pattern)
            print('!!!!!!!! search_stemmed', search_stemmed, f_search_stemmed)
            print('!!!!!!!! search_lemmatized', search_lemmatized, f_search_lemmatized)
            print('!!!!!!!! search content', cleaned_stemmed_lemmatized[0])
            print('!!!!!!!! stemmed content', cleaned_stemmed_lemmatized[1])
            print('!!!!!!!! lemmatized content',  cleaned_stemmed_lemmatized[2])


        save_search_result(mydb, repos_id, repos_meta_id, search_terms_id, f_search_pattern, f_search_stemmed, f_search_lemmatized)

        f = f_search_pattern + f_search_stemmed + f_search_lemmatized
        total_found += f

    update_repos_meta(mydb, repos_meta_id, search_terms_id)
    execution_time = datetime.now() - start_time
    print('#######', repo_row_counter, execution_time, 'Repos_id', repos_id, 'Processed row ', repos_meta_id, '  Found: ', total_found)


mydb.close()
print('TOTAL DURATION for ', limit, 'records ', datetime.now() - start_time0)
