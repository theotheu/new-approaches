import os
import pandas as pd
from datetime import datetime
import csv
import sys

start_time = datetime.now()
t = datetime.now()

csv.field_size_limit(csv.field_size_limit() * 10)

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

BASE_DIRECTORY = os.getcwd()
WORKING_DIRECTORY = BASE_DIRECTORY + '/../analysis/'
os.chdir(WORKING_DIRECTORY)

dirname = os.getcwd().split('/')
dirname.pop()
REPO_DIRECTORY = '/'.join(dirname) + '/repositories/'

repos = [f.name for f in os.scandir() if f.is_dir() and f.name != 'analysis' and f.name != '.ipynb_checkpoints']
repos.sort()

# repos = ['ansible']

max_number_of_branches_for_a_commit = 0


for d in repos:

    branches = {}

    ANALYSIS_DIRECTORY = '/'.join(dirname) + '/analysis/' + d
    os.chdir(ANALYSIS_DIRECTORY)

    print('\n------------------------------------------')
    print('>>>>> ANALYSIS_DIRECTORY: ', ANALYSIS_DIRECTORY)
    print('>>>>> Processing repository: ', d)
    df = pd.read_csv('merged.csv', delimiter=';', quotechar='"', header=0, engine='python')
    print('>>>>> shape: ', df.shape)

    WORKING_DIRECTORY = '/'.join(dirname) + '/repositories/' + d
    os.chdir(WORKING_DIRECTORY)

    for index, record in df.iterrows():

        if index > 0 and index % 100 == 0:
            print(index, datetime.now() - t, datetime.now() - start_time)
            t = datetime.now()

        hash = str(record[0])

        if len(hash) > 0:
            cmd = 'git branch -a --contains ' + hash + ' 2>/dev/null'

            stream = os.popen(cmd)
            output = stream.read()
            _branches = output.split('\n')
            _branches.pop()
            if len(_branches) > max_number_of_branches_for_a_commit:
                max_number_of_branches_for_a_commit = len(_branches)
                print('max_number_of_branches_for_a_commit: ', d, max_number_of_branches_for_a_commit, hash)
                print(_branches)

            stripped =  [s.strip() for s in _branches]
            s = '<<<SEP>>>'.join(stripped)
            branches[hash] = s

    ANALYSIS_DIRECTORY = '/'.join(dirname) + '/analysis/' + d
    os.chdir(ANALYSIS_DIRECTORY)

    print('>>>>> branches', type(branches), branches)

    # branches.to_csv('./hashes_with_branches.csv', sep=';', index=False)
    csv_file = ANALYSIS_DIRECTORY + '/hashes_with_branches.csv'
    header = ['Hash', 'Branches']
    with open(csv_file, 'w', encoding='utf-8-sig') as f:
        writer = csv.writer(f, delimiter=';', quotechar='"')
        writer.writerow(header)  # write the header
        for key, value in branches.items():
            # line = data + ';"' + branches[data] + '"'
            print ('---> ', [key, value])
            writer.writerow([key, value])

    print('Done: ', datetime.now() - start_time)
