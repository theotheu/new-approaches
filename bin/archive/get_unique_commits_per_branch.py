import os
import sys

import pandas as pd
pd.options.mode.chained_assignment = None

target = 'cpython'

dirname = os.getcwd().split('/')
dirname.pop()
REPO_DIRECTORY = '/'.join(dirname) + '/repositories/'
ANALYSIS_DIRECTORY = '/'.join(dirname) + '/analysis/' + target
os.chdir(ANALYSIS_DIRECTORY)

print("Working directory: ", os.getcwd())

files = [f.name for f in os.scandir() if 'commits_and_branches' in f.name]

numbers = []

file_counter = 1
for file in files:
    parts = file.split('.')
    numbers.append(int(parts[1]))
    numbers.sort()

    if (file_counter < len(numbers)):
        prev_file_counter = file_counter
        filename1 = 'commits_and_branches.' + str(file_counter) + '.csv'
        file_counter += 1
        filename2 = 'commits_and_branches.' + str(file_counter) + '.csv'


        df1 = pd.read_csv(filename1, sep=';')
        df2 = pd.read_csv(filename2, sep=';')

        m = df1.merge(df2, on='CommitHash', how='outer', suffixes=['', '_'], indicator=True)

        left_only = m[m['_merge'].isin(['left_only'])]

        print('\n---------------------------------------------------')
        print('>>>>> Processing ', filename1, filename2)
        print('>>>>> Unique commits for branch: ', left_only.iloc[0][1])

        left_only.drop(columns=['repo_id_', 'BranchName_', '_merge'], axis=1, inplace=True)
        left_only = left_only.astype({"repo_id": int})

        new_file_name = 'unique_commits_per_branch.' + str(prev_file_counter) + '.csv'
        print('>>>>>', left_only.shape)
        print('-----', left_only.head())
        print('----- writing to ', new_file_name)

        left_only.to_csv(new_file_name, index=False, sep=';')

        # sys.exit()
        # os.remove(filename1)
