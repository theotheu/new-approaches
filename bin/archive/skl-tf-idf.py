import mysql.connector
import re
import nltk

nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()
import pandas as pd
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

from datetime import datetime

start_time0 = datetime.now()

limit = 10
record_counter = 0
string_length_treshold = 2
stop_words = set(stopwords.words('english'))

corpus = []


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    return mydb


def parse_record(comment):
    words = []
    for w in re.sub("[^a-zA-Z]", " ", comment.lower()).split():
        if not w in stop_words and len(w) > string_length_treshold:
            words.append(w)
    return words



def calculate_binary_term_frequency():
    # https://towardsdatascience.com/getting-started-with-text-vectorization-2f2efbec6685
    tv = TfidfVectorizer (binary=True,
                          norm=False,
                          use_idf=False,
                          smooth_idf=False,
                          lowercase=True,
                          stop_words='english',
                          min_df=1,
                          max_df=1.0,
                          max_features=None,
                          ngram_range=(1,1))
    x = tv.fit_transform(corpus)
    df = pd.DataFrame(x.toarray(), columns=tv.get_feature_names)
                      #.fit_transform(corpus).toarray(), columns=tv.get_feature_names)
    print (df)



def calculate_tfidf_score():
    # https://towardsdatascience.com/tf-idf-explained-and-python-sklearn-implementation-b020c5e83275
    tfIdfVectorizer = TfidfVectorizer(use_idf=True)
    tfIdf = tfIdfVectorizer.fit_transform(corpus)
    df = pd.DataFrame(tfIdf[0].T.todense(), index=tfIdfVectorizer.get_feature_names(), columns=["TF-IDF"])
    df = df.sort_values('TF-IDF', ascending=False)
    print(df.head(25))


def get_records(mycursor):
    query = 'select id, comment from repos_meta where tf_attempts=0 limit %s'
    data = (limit,)
    # data = ()
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    record_counter = 0
    print('get_records: ', datetime.now() - start_time0)
    for repos_meta_id, comment in myresult:
        # print("################################ record", record_counter)
        record_counter = record_counter + 1
        corpus.append(comment)
    print("################################ record", record_counter)
    calculate_binary_term_frequency()


mydb = create_db_connection()
mycursor = mydb.cursor(buffered=True)
get_records(mycursor)
mycursor.close()
mydb.close()

print('TOTAL DURATION for ', limit, 'records ', datetime.now() - start_time0)
