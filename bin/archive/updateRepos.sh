#!/bin/bash

export BASE_DIRECTORY=`pwd`/..
export OLD_PWD=`pwd`
export WORKING_DIRECTORY="${BASE_DIRECTORY}/repositories"

cd $WORKING_DIRECTORY
for i in $(ls -d */); do 
	export DIR=${i%%/}
	if [ "$DIR" != 'analysis' ]; then
                echo
                echo "==============================================================================="
                cd "${WORKING_DIRECTORY}/${DIR}"
                echo ">>>>> Processing $DIR"
                git fetch --all && git reset --hard && git pull         
	fi
done
cd $OLD_PWD

