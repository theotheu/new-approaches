import pandas as pd
#from IPython.display import clear_output
from datetime import datetime
import re
import nltk
nltk.download('wordnet')
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
stop_words = set(stopwords.words('english'))
import sys
import csv

csv.field_size_limit(sys.maxsize)

filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-full-set.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/test.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-10.csv'
#filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-100.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-1.000.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-10.000.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-100.000.csv'
# filename = '/Users/theotheunissen/Google Drive/new_approaches/git-commit-messages-1000.000.csv'

w_dict = {}
s_dict = {}
l_dict = {}
wr_dict = {}
ws_dict = {}
wl_dict = {}
words = {}
string_length_treshold = 2

def save_term_tf(repos_meta_id, w, s):
    #####
    if w not in wr_dict:
        wr_dict[w] = {}

    sid = str(repos_meta_id)
    if sid not in wr_dict[w]:
        wr_dict[w][sid] = 0

    wr_dict[w][sid] += 1

    #######
    if s not in ws_dict:
        ws_dict[s] = {}

    sid = str(repos_meta_id)
    if sid not in ws_dict[s]:
        ws_dict[s][sid] = 0

    ws_dict[s][sid] += 1

def parse_record(repos_meta_id, comment):
    for w in re.sub("[^a-zA-Z]", " ", str(comment).lower()).split():
        if not w in stop_words and len(w) > string_length_treshold:
            s = stemmer.stem(w)
            l = wordnet_lemmatizer.lemmatize(w)
            words[w] = [s, l]
            save_term_tf(repos_meta_id, w, s)

start_time0 = datetime.now()

print ('Using filename:', filename)
df = pd.read_csv(filename, delimiter=';', quotechar='"', header=1, engine='python' ) # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False
print (df.shape)
print('Reading filename: ', datetime.now() - start_time0)

data = []

for index, row in df.iterrows():
  repos_meta_id=row[0]
  comment=row[1]
 # data.append(comment)

  parse_record(repos_meta_id, comment)

#print ('>>>>>', ws_dict)

print('TOTAL DURATION: ', datetime.now() - start_time0)