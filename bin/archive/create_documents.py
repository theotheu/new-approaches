import pandas as pd
from datetime import datetime
import numpy as np
import mysql.connector
import sys
import csv

start_time = datetime.now()


def unique(list1):
    x = np.array(list1)
    return (np.unique(x))


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches',
    )
    return mydb


def read_file_from_disk(dir, in_filename, out_filename):
    mydb = create_db_connection()
    hashes_with_branches = {}
    branches_with_hashes = {}

    filename = dir + in_filename

    df = pd.read_csv(filename, delimiter=';', quotechar='"', header=0,
                     engine='python')  # , header=True, index_col=0, squeeze=True)
    print(df.shape)
    for index, row in df.iterrows():
        hash = row[0]
        if type(row[1]) == str:
            if index % 10000 == 0:
                print('another batch: ', format(index), '/', format(df.shape[0]), datetime.now() - start_time)

            branches = row[1].split('<<<SEP>>>')
            hashes_with_branches[hash] = branches
            for branch in branches:
                if branch not in branches_with_hashes:
                    branches_with_hashes[branch] = []
                # if hash not in branches_with_hashes[branch]:
                branches_with_hashes[branch].append(hash)

    # print(len(hashes_with_branches))
    # print(hashes_with_branches['3642c401b67e051a6a7a31db627b8099e42318d9'])
    print(len(branches_with_hashes))
    filename = dir + out_filename
    f = open(filename, 'w')
    # create the csv writer
    writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

    row = ['Branch', 'Document']
    # write a row to the csv file
    writer.writerow(row)
    f.close()

    for branch in branches_with_hashes:
        l = unique(branches_with_hashes[branch])
        print('another branch: ', len(l), branch, datetime.now() - start_time)
        mycursor = mydb.cursor()

        query = 'select comment from repos_meta where hash in ("'
        query += '","'.join(l)
        query += '") '
        query += 'order by comitterDate'
        data = ()
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        mycursor.close()

        b = [i for sub in myresult for i in sub]
        c = ' '.join(b)
        c = c.replace('\n', ' ')

        mycursor.close()

        row = [branch, c]

        f = open(filename, 'a')
        # create the csv writer
        writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

        # write a row to the csv file
        writer.writerow(row)

        # close the file
        f.close()

    mydb.close()


dir = '/Volumes/1tb/analysis/angular/'
in_filename = 'hashes_with_branches.csv'
out_filename = 'documents.csv'

read_file_from_disk(dir, in_filename, out_filename)
