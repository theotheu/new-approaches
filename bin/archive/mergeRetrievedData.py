import os
import pandas as pd
import glob
from functools import reduce

import csv
csv.field_size_limit(csv.field_size_limit() * 10)

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


BASE_DIRECTORY= os.getcwd()
WORKING_DIRECTORY=BASE_DIRECTORY + '/../analysis/'
os.chdir(WORKING_DIRECTORY)

repos = [f.name for f in os.scandir() if f.is_dir() and f.name != 'analysis' and f.name != '.ipynb_checkpoints']
repos.sort()

repos=['ansible']

for d in repos:
    path = r'' + WORKING_DIRECTORY + d + '/'
    all_files = glob.glob(path + "/*.csv")

    li = []

    print ("\nRepository " + path)

    for filename in all_files:
        if 'merged.csv' not in filename:
            print (">>>>> " + filename)
            df = pd.read_csv(filename, delimiter=';', quotechar='"', header=0, engine='python' ) # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False
            print (df.shape)
            li.append(df)
            frame = pd.concat(li, axis=0, ignore_index=True)
            merged = reduce(lambda left, right: pd.merge(left, right, on='hash'), li)
            merged.to_csv(path + '/merged.csv', sep=';', index=False)
            
            # for index, row in df.iterrows():
            #    print ("\n", index, row)
            

    



