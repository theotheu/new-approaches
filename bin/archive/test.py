from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd

corpus = ["This is a brown house. This house is big. The street number is 1.",
          "This is a small house. This house has 1 bedroom. The street number is 12.",
          "This dog is brown. This dog likes to play.",
          "The dog is in the bedroom."]

def set_pandas_display_options() -> None:
    """Set pandas display options."""
    # Ref: https://stackoverflow.com/a/52432757/
    display = pd.options.display

    display.max_columns = 1000
    display.max_rows = 1000
    display.max_colwidth = 199
    display.width = 1000
    # display.precision = 2  # set as needed

set_pandas_display_options()


tv = TfidfVectorizer(
    binary=False,
    norm='l2',
    use_idf=True,
    smooth_idf=True,
    lowercase=True,
    stop_words='english',
    min_df=1,
    max_df=1.0,
    max_features=None,
    ngram_range=(1, 1)
)

df = pd.DataFrame(tv.fit_transform(corpus).toarray(), columns=tv.get_feature_names())
with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    print(df)

pd.set_option('display.max_columns', None)  # or 1000
pd.set_option('display.max_rows', None)  # or 1000
pd.set_option('display.max_colwidth', None)  # or 199

# print(df)
