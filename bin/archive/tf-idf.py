import mysql.connector
import re
import nltk

nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()
from datetime import datetime

start_time0 = datetime.now()

limit = 1000
cache_hits = 0
cache_misses = 0
record_counter = 0
string_length_treshold = 2
stop_words = set(stopwords.words('english'))

w_dict = {}
s_dict = {}
l_dict = {}
wr_dict = {}
sr_dict = {}
lr_dict = {}


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches'
    )
    return mydb


def save_stemmed_tf(mycursor, repos_meta_id, w, s, l):
    #####
    global cache_hits
    global cache_misses
    table_name_w = 'words_stemmed'
    table_name_i = 'messages_with_words_stemmed'
    #####
    if s in s_dict:
        words_id = s_dict[s]
        cache_hits = cache_hits + 1
    else:
        query = 'select id from ' + table_name_w + ' where stemmed=%s'
        data = (s,)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount
        if count == 0:
            cache_misses = cache_misses + 1
            query = 'insert into ' + table_name_w + ' (stemmed) values '
            query += '(%s)'
            data = (s,)
            # print(query, data)
            mycursor.execute(query, data)
            mydb.commit()
            words_id = mycursor.lastrowid
        else:
            words_id = myresult[0][0]

    key = str(repos_meta_id) + '|' + str(words_id)
    if key in sr_dict:
        mwrp_id = sr_dict[key]
        cache_hits = cache_hits + 1
    else:
        query = 'select id from ' + table_name_i + ' where words_stemmed_id=%s and repos_meta_id=%s'
        data = (words_id, repos_meta_id,)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount
        if count == 0:
            cache_misses = cache_misses + 1
            query = 'insert into ' + table_name_i + ' (words_stemmed_id, repos_meta_id, found_in_message) values '
            query += '(%s, %s, %s)'
            data = (words_id, repos_meta_id, 0,)
            # print(query, data)
            mycursor.execute(query, data)
            mydb.commit()
            mwrp_id = mycursor.lastrowid
        else:
            mwrp_id = myresult[0][0]

    query = 'update ' + table_name_i + ' set '
    query += 'found_in_message=found_in_message+1 '
    query += 'where id=%s'
    data = (mwrp_id,)
    # print(query, data)
    mycursor.execute(query, data)
    mydb.commit()

    s_dict[s] = words_id
    sr_dict[key] = mwrp_id


def save_term_tf(mycursor, repos_meta_id, w, s, l):
    #####
    global cache_hits
    global cache_misses
    if w in w_dict:
        words_plain_id = w_dict[w]
        cache_hits = cache_hits + 1
    else:
        query = 'select id from words_plain where word=%s'
        data = (w,)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount
        if count == 0:
            cache_misses = cache_misses + 1
            query = 'insert into words_plain (word) values '
            query += '(%s)'
            data = (w,)
            # print(query, data)
            mycursor.execute(query, data)
            mydb.commit()
            words_plain_id = mycursor.lastrowid
        else:
            words_plain_id = myresult[0][0]

    key = str(repos_meta_id) + '|' + str(words_plain_id)
    if key in wr_dict:
        mwrp_id = wr_dict[key]
        cache_hits = cache_hits + 1
    else:
        query = 'select id from messages_with_words_plain where words_plain_id=%s and repos_meta_id=%s'
        data = (words_plain_id, repos_meta_id,)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount
        if count == 0:
            cache_misses = cache_misses + 1
            query = 'insert into messages_with_words_plain (words_plain_id, repos_meta_id, found_in_message) values '
            query += '(%s, %s, %s)'
            data = (words_plain_id, repos_meta_id, 0,)
            # print(query, data)
            mycursor.execute(query, data)
            mydb.commit()
            mwrp_id = mycursor.lastrowid
        else:
            mwrp_id = myresult[0][0]

    query = 'update messages_with_words_plain set '
    query += 'found_in_message=found_in_message+1 '
    query += 'where id=%s'
    data = (mwrp_id,)
    # print(query, data)
    mycursor.execute(query, data)
    mydb.commit()

    w_dict[w] = words_plain_id
    wr_dict[key] = mwrp_id

    # print(words_plain_id, mwrp_id, w, s, l)


def parse_record(mycursor, repos_meta_id, comment):
    for w in re.sub("[^a-zA-Z]", " ", comment.lower()).split():
        if not w in stop_words and len(w) > string_length_treshold:
            s = stemmer.stem(w)
            l = wordnet_lemmatizer.lemmatize(w)
            save_term_tf(mycursor, repos_meta_id, w, s, l)
            save_stemmed_tf(mycursor, repos_meta_id, w, s, l)
            query = 'update repos_meta set tf_attempts=tf_attempts+1, modification_date=now() where id=%s'
            data = (repos_meta_id,)
            mycursor.execute(query, data)
            mydb.commit()


def get_records(mycursor):
    global cache_hits
    global cache_misses
    query = 'select id, comment from repos_meta where tf_attempts=0 limit %s'
    data = (limit,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    record_counter = 0
    for id, comment in myresult:
        print("################################ record", record_counter)
        print("cache misses/hits: ", cache_misses, cache_hits)
        record_counter = record_counter + 1
        parse_record(mycursor, id, comment)


mydb = create_db_connection()
mycursor = mydb.cursor(buffered=True)
get_records(mycursor)
mycursor.close()
mydb.close()

print('TOTAL DURATION for ', limit, 'records ', datetime.now() - start_time0)
