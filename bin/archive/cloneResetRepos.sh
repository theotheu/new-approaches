#!/bin/bash

export BASE_DIRECTORY=`pwd`/..
export OLD_PWD=`pwd`
export WORKING_DIRECTORY="${BASE_DIRECTORY}/repositories"

for i in $(ls -d */); do 
	DIR=${i%%/}
	if [ "$DIR" != 'analysis' ]; then
		echo
		echo "================================================================"
		echo "Processing $DIR"
	        cd "${WORKING_DIRECTORY}/${DIR}"
        	git fetch --all && git reset --hard && git pull
       		cd -
	fi
done

