import sys
from datetime import datetime
from csv import reader
import re
import nltk
import csv
import sys
import math
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
import numpy as np
import hashlib

nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
csv.field_size_limit(sys.maxsize)
start_time = datetime.now()

Nt_plain_dict = {}
Nt_stemmed_dict = {}
FT_plain_dict = {}
FT_stemmed_dict = {}
number_of_documents = 0
plain_words_per_document = {}

start_time = datetime.now()
minimal_word_length = 2

dir = '/Users/theotheunissen/workspaces/new-approaches/analysis/WordPress/'
dir = '/Users/theotheunissen/workspaces/new-approaches/analysis/_test/'
in_filename = 'documents.csv'



def unique(list1):
    x = np.array(list1)
    return (np.unique(x))


def cleaner_stemmer_lemmatizer(key, field):
    # https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-soFT_ware-engineering-use-case-779d4a28ba5

    global minimal_word_length

    field = field.lower()
    words_only = []
    stemmed = []
    lemmatized = []

    sanitized_words = re.sub("[^a-zA-Z]", " ", field).split()
    word_counter = 0

    for w in sanitized_words:
        sw = stemmer.stem(w)
        lw = wordnet_lemmatizer.lemmatize(w)
        words_only.append(w)
        stemmed.append(sw)
        lemmatized.append(lw)

        if w not in Nt_plain_dict:
            Nt_plain_dict[w] = 0
        if w not in FT_plain_dict[key]:
            FT_plain_dict[key][w] = 0
        if sw not in Nt_stemmed_dict:
            Nt_stemmed_dict[sw] = 0
        if sw not in FT_stemmed_dict[key]:
            FT_stemmed_dict[key][sw] = 0

        if len(w) >= minimal_word_length:
            word_counter += 1
            Nt_plain_dict[w] += 1
            Nt_stemmed_dict[sw] += 1
            FT_plain_dict[key][w] += 1
            FT_stemmed_dict[key][sw] += 1

    unique_words_only = unique(words_only)
    unique_stemmed = unique(stemmed)
    unique_lemmatized = unique(lemmatized)

    retVal = [words_only, stemmed, lemmatized, len(unique_words_only), len(unique_stemmed), len(unique_lemmatized), word_counter]

    return retVal


def read_file_from_disk(dir, in_filename):
    global number_of_documents

    document_hashes = []

    filename = dir + in_filename

    print ('>>>>> Reading from : ', filename)

    # open file in read mode
    with open(filename, 'r') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj, delimiter=';', quotechar='"')
        # Iterate over each row in the csv using reader object
        row_counter = 0
        for row in csv_reader:
            if row_counter > 0:
                branch = row[0]
                document = row[1]
                md5 = hashlib.md5(document.encode()).hexdigest()

                if md5 not in document_hashes:
                    document_hashes.append(md5)

                    FT_plain_dict[branch] = {}
                    FT_stemmed_dict[branch] = {}

                    # row variable is a list that represents a row in csv
                    r = cleaner_stemmer_lemmatizer(branch, document)
                    plain_words_per_document[branch] = r[3]
                    print('\n----- branch                            : ', branch)
                    print('----- number of documents               : ', '{:,.0f}'.format(number_of_documents))
                    print('----- unique plain words for document   : ', '{:,.0f}'.format(len(Nt_plain_dict)))
                    print('----- unique stemmed words for document : ', '{:,.0f}'.format(len(Nt_stemmed_dict)))
                    print('----- number of plain words             : ', '{:,.0f}'.format(r[3]))
                    print('----- number of stemmed words           : ', '{:,.0f}'.format(r[4]))
                    print('----- number of lemmatized words        : ', '{:,.0f}'.format(r[5]))
                    print('----- total number of words for document: ', '{:,.0f}'.format(r[6]))
                    print('----- duration: ', start_time.now() - start_time, start_time.now())
                    number_of_documents += 1
            row_counter += 1


read_file_from_disk(dir, in_filename)

print('\n>>>>> number of documents: ', '{:,.0f}'.format(number_of_documents))
print('>>>>> unique plain words for all documents: ', '{:,.0f}'.format(len(Nt_plain_dict)))
print('>>>>> unique stemmed words for all documents: ', '{:,.0f}'.format(len(Nt_stemmed_dict)))
print('>>>>> duration: ', start_time.now() - start_time, start_time.now())


def save_tfidf():
    all_words_for_all_documents = len(Nt_plain_dict)
    for w in Nt_plain_dict:
        print ('\n------------------------')
        print ('w:  ', w)
        Nt = Nt_plain_dict[w]
        print('Nt: ', Nt)

        for b in FT_plain_dict:
            print ('d:  ', b)

save_tfidf()
sys.exit()


# #########################
filename = dir + 'tf-idf.csv'
f = open(filename, 'w')
# create the csv writer
writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

# Write header
# https://dataaspirant.com/tf-idf-term-frequency-inverse-document-frequency/
row = ['Branch', 'Word', 'Σf(t,d)', 'Term-Frequency (TF)', 'Nt', 'N/Nt', 'IDF', 'TF-IDF']
# write a row to the csv file
writer.writerow(row)

for b in FT_plain_dict:
    all_words_per_document = plain_words_per_document[b]
    for w in FT_plain_dict[b]:
        unique_words = FT_plain_dict[b][w]
        tf = unique_words / all_words_per_document

        Nt = Nt_plain_dict[w]
        nnt = number_of_documents / (1+Nt_plain_dict[w])
        idf = -math.log10(nnt)
        tfidf = tf * idf

        print ('\n------------------------')
        print ('d : ', b)
        print ('w : ', w)
        print ('N : ', number_of_documents)
        print ('Nt: ', Nt)

        row = [b, w, FT_plain_dict[b][w], str(tf), str(Nt), str(nnt), str(idf), str(tfidf)]
        writer.writerow(row)
# close the file
f.close()

# #########################
filename = dir + 'Nt.csv'
f = open(filename, 'w')
# create the csv writer
writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

# Write header
# https://dataaspirant.com/tf-idf-term-frequency-inverse-document-frequency/
row = ['Word', 'Nt']
# write a row to the csv file
writer.writerow(row)

for w in Nt_plain_dict:
    row = [w, Nt_plain_dict[w]]
    writer.writerow(row)
# close the file
f.close()
