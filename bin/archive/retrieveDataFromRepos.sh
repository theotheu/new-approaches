#!/bin/bash


export NO_PAGER="--no-pager"

export BASE_DIRECTORY=`pwd`/..
export OLD_PWD=`pwd`
export REPO_DIRECTORY="${BASE_DIRECTORY}/repositories"
export BIN_DIRECTORY="${BASE_DIRECTORY}/bin"
export ANALYSIS_DIRECTORY="${BASE_DIRECTORY}/analysis"

export LC_ALL=C

# rm -fr ./analysis

export I=

cd $REPO_DIRECTORY

for i in $(ls -d */); do 
	DIR=${i%%/}
	if [ "$DIR" == 'ansible' ]; then
		echo
		echo "==============================================================================="
		cd $DIR
		echo ">>>>> Processing repo $DIR"
		OUT_DIR="${BASE_DIRECTORY}/analysis/${DIR}"
		mkdir -p $OUT_DIR
		rm -fr $OUT_DIR/*
		echo ">>>>> OUT_DIR: $OUT_DIR"

		# Got warnings in linux repo
		git config diff.renameLimit 999999
		git config merge.renameLimit 999999
		
		TMP_FILE="/tmp/new-approaches.csv"

		TMP_FILE="/tmp/messages.csv"
		OUT_FILE="${OUT_DIR}/messages.csv"
		echo "hash;parentHashes;author;authorDate;comitter;comitterDate;subject;comment" > $TMP_FILE
		git $NO_PAGER log --date=local --all  --pretty="%H
%x3B%P
%x3B%an
%x3B%ad
%x3B%cn
%x3B%cd
%x3BCOMMENTMARK%sCOMMENTMARK
%x3BCOMMENTMARK%BCOMMENTMARK" | tr '"' "'" |  tr "\r;" ';' >> $TMP_FILE
		perl -0777 -i -0pe 's/\n;/;/smg' $TMP_FILE
#		sed $I '' 's/COMMENTMARK/\\\"/g' $TMP_FILE
		perl -0777 -i -0pe 's/COMMENTMARK/"/smg' $TMP_FILE
	
		mv $TMP_FILE $OUT_FILE

		OUT_FILE="${OUT_DIR}/stats.csv"
		TMP_FILE="/tmp/stats.csv"
		echo "hash;changedFiles;linesAdded;linesDeleted" > $TMP_FILE
		git $NO_PAGER log --date=local --all  --pretty="NEWLINE%H%x3B" --shortstat | tr "\n" " " | tr "@" "\n"  >> $TMP_FILE

		perl -0777 -i -0pe 's/^NEWLINE//smg' $TMP_FILE
		perl -0777 -i -0pe 's/NEWLINE/\n/smg' $TMP_FILE
		perl -0777 -i -0pe 's/, /;/smg' $TMP_FILE
		perl -0777 -i -0pe 's/ files changed//smg' $TMP_FILE
		perl -0777 -i -0pe 's/ file changed//smg' $TMP_FILE
		perl -0777 -i -0pe 's/ insertions\(\+\)//smg' $TMP_FILE
		perl -0777 -i -0pe 's/ insertion\(\+\)//smg' $TMP_FILE
		perl -0777 -i -0pe 's/deletions\(-\)//smg' $TMP_FILE
		perl -0777 -i -0pe 's/deletion\(-\)//smg' $TMP_FILE
		perl -0777 -i -0pe 's///smg' $TMP_FILE
		perl -0777 -i -0pe 's/;   /;/smg' $TMP_FILE
		#sed $I '' 's/^NEWLINE//g' $TMP_FILE
#		sed $I '' 's/NEWLINE/\n/g' $TMP_FILE
#		sed $I '' 's/, /;/g' $TMP_FILE
#		sed $I '' 's/ file changed//g' $TMP_FILE
#		sed $I '' 's/ files changed//g' $TMP_FILE
#		sed $I '' 's/ insertions(+)//g' $TMP_FILE
#		sed $I '' 's/ insertion(+)//g' $TMP_FILE
#		sed $I '' 's/ deletions(-)//g' $TMP_FILE
#		sed $I '' 's/ deletion(-)//g' $TMP_FILE
#		sed $I '' 's/ //g' $TMP_FILE
		
		mv $TMP_FILE $OUT_FILE
		cd -
	fi
done

#cd $BIN_DIRECTORY
#
## pyenv install -v 3.7.9
#pyenv global 3.7.9
## pip freeze > requirements.txt
#pip install -r requirements.txt
#
#python mergeRetrievedData.py