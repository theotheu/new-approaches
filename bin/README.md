

1. `./1.clone_repos.sh`
   - This is an initial process and typically runs only once.
1. `./2.cloneResetRepos.sh`
   - This process can be run to update the repositories.
1. `./3.getCommitMessagesFromRepositories.sh`
   - This process retrieves commit messages (comments) and stats.
   - The result is two files: `messages.csv` and `stats.csv` 
1. `python 4.add_branch_to_merged.py`
   - Read hashes from `messages.csv` to get branches.
   - The result is `hashes_with_branches.csv`
1. `python 5.create_documents.py`
   - Creates a document per branch with all commit messages.
   - The result is `documents.csv`
1. `python 6.calculate_tf-idf.py`
   - Calculates the TF-IDF per word.
   - The results are stored in `tf-idf.csv`
   


# Optional
1. Optional: `python mergeRetrievedData.py`
   - This process merges the two file into one file.
   - The result is `merged.csv` 