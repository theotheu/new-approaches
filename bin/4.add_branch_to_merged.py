import os
import pandas as pd
from datetime import datetime
import csv
import sys
import argparse

start_time = datetime.now()
t = datetime.now()

csv.field_size_limit(csv.field_size_limit() * 10)

# https://stackoverflow.com/questions/15063936/csv-error-field-larger-than-field-limit-131072
maxInt = sys.maxsize

while True:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.

    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)


pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')

args = parser.parse_args()

repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR='/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print ('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print ('ANALYSIS_DIR :', ANALYSIS_DIR)


#repos = ['bitcoin', 'brew']

max_number_of_branches_for_a_commit = 0


branches = {}

os.chdir(ANALYSIS_DIR)

print('\n------------------------------------------')
print('>>>>> ANALYSIS_DIR: ', ANALYSIS_DIR)
print('>>>>> Processing repository: ', repo_name)
df = pd.read_csv('messages.csv', delimiter=';', quotechar='"', header=0, engine='python', encoding='utf-8', encoding_errors='ignore')
number_of_work_items = df.shape[0]
print('>>>>> shape: ', number_of_work_items)

os.chdir(SOURCE_DIR)

for index, record in df.iterrows():

    if index > 0 and index % 100 == 0:
        ts = datetime.now() - start_time
        ts=ts.total_seconds()
        todo = (number_of_work_items - index)   * (ts/index)
        print(repo_name, index, datetime.now() - t, datetime.now() - start_time, '; seconds=', round(todo), '; minutes=', round(todo/60,2),'; hours=', round(todo/3600,2), '; day =', round(todo/3600/24, 2))
        t = datetime.now()

    hash = str(record[0])

    if len(hash) > 0:
        cmd = 'git branch -a --contains ' + hash + ' 2>/dev/null'

        stream = os.popen(cmd)
        output = stream.read()
        _branches = output.split('\n')
        _branches.pop()
        if len(_branches) > max_number_of_branches_for_a_commit:
            max_number_of_branches_for_a_commit = len(_branches)
            print('max_number_of_branches_for_a_commit: ', repo_name, max_number_of_branches_for_a_commit, hash)
            print(_branches)

        stripped =  [s.strip() for s in _branches]
        s = '<<<SEP>>>'.join(stripped)
        branches[hash] = s

os.chdir(ANALYSIS_DIR)

print('>>>>> branches', type(branches), branches)

# branches.to_csv('./hashes_with_branches.csv', sep=';', index=False)
csv_file = ANALYSIS_DIR + '/hashes_with_branches.csv'
header = ['Hash', 'Branches']
with open(csv_file, 'w', encoding='utf-8-sig') as f:
    writer = csv.writer(f, delimiter=';', quotechar='"')
    writer.writerow(header)  # write the header
    for key, value in branches.items():
        # line = data + ';"' + branches[data] + '"'
        # print ('---> ', [key, value])
        writer.writerow([key, value])

print('Done: ', datetime.now() - start_time)
