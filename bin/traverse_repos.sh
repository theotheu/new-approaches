#!/bin/bash

export BASE_DIRECTORY=`pwd`/..
export OLDPWD=`pwd`

export WORKING_DIRECTORY="${BASE_DIRECTORY}/repositories"
cd $WORKING_DIRECTORY
WORKING_DIRECTORY=`pwd`
echo "Working directory: $WORKING_DIRECTORY"

for i in $(ls -d */); do
	DIR=${i%%/}
		echo
		echo "================================================================"
		echo "Processing: $DIR"
    SOURCE_DIR="${WORKING_DIRECTORY}/$DIR"
    cd $SOURCE_DIR
    S=`pwd`
		echo "SOURCE_DIR: $WORKING_DIRECTORY, $S "

 		cd -
done
