import pandas as pd
import math
from datetime import datetime

start_time = datetime.now()

print('Start reading from disk ', datetime.now() - start_time)
dtf = pd.read_csv("archive/OUT_PLAIN_export_dtf_is_the_number_of_times_the_term_appears_in_the_document.csv",
                  sep='\t',
                  header=None, names=['repos_meta_id', 'word', 'found'])
print('finished reading from disk ', datetime.now() - start_time)
# U = pd.read_csv("./OUT_U_is_the_number_of_Unique_terms_in_the_document_PLAIN_STEMMED.csv", sep='\t', header=None)

print('--------')
column_values = dtf[['repos_meta_id']].values.ravel()
unique_repos_meta_ids = pd.unique(column_values)
print('selecting unique rows for respos_meta_id ', datetime.now() - start_time)
dtf.sort_values(by=['repos_meta_id'], inplace=True)
print('sorting by repos_meta_id ', datetime.now() - start_time)

print('--------')
selection = dtf
row_count = selection.shape[0]
print('-------- unique_repos_meta_ids', len(unique_repos_meta_ids))

print('-------- row_count', row_count)
print('-------- sum', selection['found'].sum())

bow_total_column = []
bow_unique_column = []

#####
l = len(unique_repos_meta_ids)
counter = 1
for repos_meta_id in unique_repos_meta_ids:
    if (counter % 1000 == 0):
        s = datetime.now() - start_time
        todo = (l - counter) * (s.total_seconds() / counter)
        print('step 1, unique_repos_meta_ids: ', datetime.now() - start_time, counter, l, round(todo/3600/24, 2))
    counter += 1
    selection = dtf[dtf['repos_meta_id'] == repos_meta_id]
    unique_number_of_words = selection.shape[0]
    total_number_of_words = selection['found'].sum()
    for i in range(0, unique_number_of_words):
        bow_total_column.append(total_number_of_words)
        bow_unique_column.append(unique_number_of_words)

print('start merging: ', datetime.now() - start_time)
dtf['bow_total'] = bow_total_column
dtf['bow_unique'] = bow_unique_column
print('done merging: ', datetime.now() - start_time)

### House cleaning
print('start house cleaning: ', datetime.now() - start_time)
del (unique_repos_meta_ids)
del (bow_total_column)
del (bow_unique_column)
print('done house cleaning: ', datetime.now() - start_time)

dtf.sort_values(by=['word'], inplace=True)
column_values = dtf[['word']].values.ravel()
unique_words = pd.unique(column_values)

n = dtf.shape[0]
new_column_n = []
new_column_nt = []
new_column_nt_normalized = []
new_column_n_divided_by_nt = []
new_column_idf_log10 = []
new_column_log2 = []
new_column_tf_idf = []
number_of_documents = dtf.shape[0]
l = len(unique_words)
counter = 0
start_time1 = datetime.now()
for word in unique_words:
    if (repos_meta_id % 100 == 0):
        s = datetime.now() - start_time1
        todo = (l - counter) * (s.total_seconds() / counter)
        print('step 2, w, n: ', datetime.now() - start_time1, counter, l, round(todo/3600/24, 2))
    counter += 1
    selection = dtf[dtf['word'] == word]
    number_of_document_where_term_appears = selection.shape[0]
    n_divided_by_nt = n / number_of_document_where_term_appears
    nt_normalized_10 = math.log10(1 + number_of_document_where_term_appears)
    log2 = math.log10(n_divided_by_nt)
    log10 = math.log10(nt_normalized_10)
    # print('===== unique_words', unique_words)
    for i in range(0, number_of_document_where_term_appears):
        new_column_nt.append(number_of_document_where_term_appears)
        new_column_n_divided_by_nt.append(n_divided_by_nt)
        new_column_n.append(n)
        new_column_log2.append(log2)
        new_column_idf_log10.append(log10)

print('start merging: ', datetime.now() - start_time)
dtf['n'] = new_column_n
print('1. n: ', datetime.now() - start_time)
dtf['nt'] = new_column_nt
print('2. nt: ', datetime.now() - start_time)
dtf['n_divided_by_nt'] = new_column_n_divided_by_nt
print('3. n_divided_by_nt: ', datetime.now() - start_time)
dtf['log2'] = new_column_log2
print('4. log2: ', datetime.now() - start_time)
dtf['idf_log10'] = new_column_idf_log10
print('4. idf_log10: ', datetime.now() - start_time)
print('done merging: ', datetime.now() - start_time)

print(dtf.head())

filename =  'mysql_2.csv'
dtf.to_csv(filename, index=False, sep=';', header=True)

