#!/usr/bin/env python
import dbconfig as cfg
import mysql.connector

print(cfg.mysql)

if cfg.mysql["host"] == 5.7:
    print (5.7)
else:
    cnx = mysql.connector.connect(
        host=cfg.mysql["host"],
        user=cfg.mysql["user"],
        password=cfg.mysql["password"],
        database=cfg.mysql["db"],
        auth_plugin='mysql_native_password'
    )

print (cnx)