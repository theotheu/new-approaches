#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
Linux*) PYTHON_VERSION=python3 ;;
Darwin*) PYTHON_VERSION=python ;;
*) machine="UNKNOWN:${unameOut}" ;;
esac

echo "Running on ${unameOut} with python=${PYTHON_VERSION}"

# https://unix.stackexchange.com/questions/129391/passing-named-arguments-to-shell-scripts
while [ $# -gt 0 ]; do
  case "$1" in
  -r | -repo | --repo)
    REPO="$2"
    ;;
  -a | -arg_1 | --arg_1)
    arg_1="$2"
    ;;
  *)
    printf "***************************\n"
    printf "* Error: Invalid argument.*\n"
    printf "***************************\n"
    exit 1
    ;;
  esac
  shift
  shift
done

export LC_ALL=C

export BASE_DIRECTORY=$(pwd)/..
export OLDPWD=$(pwd)

export WORKING_DIRECTORY="${BASE_DIRECTORY}/repositories"
cd $WORKING_DIRECTORY
WORKING_DIRECTORY=$(pwd)
echo "Working directory: $WORKING_DIRECTORY"

process_all_repos() {
  for i in $(ls -d */); do
    DIR=${i%%/}
    process_single_repo $DIR
   done
}

step_1() {
  ./1.clone_repos.sh
}

step_2() {
  ./2.cloneResetRepos.sh
}
step_30() {
   LOG_FILE="_${1}.3.0.out"
   rm -fr $LOG_FILE
  ./3.0.getCommitMessagesFromRepositories.sh $1  > $LOG_FILE
}

step_31() {
   LOG_FILE="_${1}.3.1.out"
   rm -fr $LOG_FILE
   $PYTHON_VERSION -u 3.1.importLogsIntoMysql.py -r $1 > $LOG_FILE
}

step_4() {
  LOG_FILE="_${1}.4.out"
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u 4.add_branch_to_merged.py -r $1 > $LOG_FILE
}

step_5() {
  LOG_FILE="_${1}.5.out"
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u 5.create_documents.py -r $1 > $LOG_FILE
}

step_6() {
  LOG_FILE="_${1}.6.out"
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u 6.calculate_tf-idf.py -r $1 > $LOG_FILE
}

step_A1() {
  LOG_FILE="_${1}.A1.out"
  rm -fr $LOG_FILE
  echo "Running: $PYTHON_VERSION -u A1.search_mem.py -r $1 > $LOG_FILE"
  $PYTHON_VERSION -u A1.search_mem.py -r $1 > $LOG_FILE
}

step_A2() {
  LOG_FILE="_${1}.A2.out"
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u A2.calculate_tf-idf.py -r $1 > $LOG_FILE
}

step_A3() {
  LOG_FILE="_${1}.A3.out"
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u A3.import_search_results_into_database.py -r $1 > $LOG_FILE
}

process_single_repo() {
  echo "================================================================"
  REPO_DIR="${WORKING_DIRECTORY}/${1}"
  REPO_DIR=$OLDPWD
  cd $REPO_DIR
  echo "Processing: $1"
  echo "WORKING_DIRECTORY: ${REPO_DIR}"
  echo "See the \"_${1}.<bash|python-file>.out\" files for the output"

#  step_1
#  step_2
#  step_30 $1
#  step_31 $1
#  step_4 $1
#  step_5 $1
#  step_6 $1
  step_A1 $1
#  step_A2 $1
#  step_A3 $1

  cd $OLDPWD
}

if [ "$REPO" != '' ]; then
  process_single_repo $REPO
else
  process_all_repos
fi
