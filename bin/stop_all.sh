#!/bin/bash

echo "Killing shell processes..."
kill -9 `ps -aef|grep -v grep|grep 0.process_repos.sh |awk {'print $2'}`


echo "Killing python processes..."
kill -9 `ps -aef|grep -v grep |grep -v Dropbox|grep python|awk {'print $2'}`