import mysql.connector
import sys
from datetime import datetime
import re
import nltk
import csv
import sys
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

import os
import argparse

nltk.download('wordnet')
nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
csv.field_size_limit(sys.maxsize)
start_time = datetime.now()

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')
parser.add_argument('--limit', '-l', help="Number of retrieved records", type=int, default=10000000)

args = parser.parse_args()

repo_name = args.repository
limit = args.limit

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR='/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print ('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print ('ANALYSIS_DIR :', ANALYSIS_DIR)


#limit = 100


start_time = datetime.now()

minimal_word_length = 2

found_plain = {}
found_stemmed = {}
found_lemmatized = {}
results = {}


# create db
def create_db_connection():z