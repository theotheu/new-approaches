import mysql.connector
import sys
from datetime import datetime
import re
import nltk
import csv
import sys
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
import argparse
import os
import dbconfig as cfg

nltk.download('wordnet')
nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
csv.field_size_limit(sys.maxsize)
start_time = datetime.now()

# limit = 100
limit = 10000000

start_time = datetime.now()

minimal_word_length = 2

found_plain = {}
found_stemmed = {}
found_lemmatized = {}
results = {}

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')
parser.add_argument('--limit', '-l', help="Limit", type=int, default='1000000000')

args = parser.parse_args()

repo_name = args.repository
limit = args.limit

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR = '/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print('ANALYSIS_DIR :', ANALYSIS_DIR)

# create db
def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password'
        )
    return mydb


def get_search_terms():
    print('\n>>>>> Start reading search terms')
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)
    query = 'select id, pattern, stemmed,lemmatized from search_terms where is_active = 1'
    data = ()
    print('>>>>> query: ', query)
    print('>>>>> data: ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print('>>>>> Length of list ' + str('{:,.0f}'.format(len(myresult))))
    return myresult


def get_repo_rows():
    print('\n>>>>> Start reading ' + str(limit) + ' records')
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)
    query = 'select m.id, m.repos_id, m.`comment` from repos_meta m , repos r where r.`name`=%s and r.id=m.repos_id limit %s'
    data = (repo_name, limit, )
    print('>>>>> query: ', query)
    print('>>>>> data: ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print('>>>>> Length of list ' + str('{:,.0f}'.format(len(myresult))))
    return myresult


def check_if_key_exist(repos_meta_id, search_terms_id, repos_id):
    if repos_meta_id not in results:
        results[str(repos_meta_id)] = {}
        results[str(repos_meta_id)]['repos_id'] = repos_id
    if 'k' + str(search_terms_id) not in results[str(repos_meta_id)]:
        results[str(repos_meta_id)]['k' + str(search_terms_id)] = {}
        results[str(repos_meta_id)]['k' + str(search_terms_id)]['patterns_found'] = 0
        results[str(repos_meta_id)]['k' + str(search_terms_id)]['stemmed_found'] = 0
        results[str(repos_meta_id)]['k' + str(search_terms_id)]['lemmatized_found'] = 0


def search_terms_in_records(search_term_counter, terms, records):
    # https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-soFT_ware-engineering-use-case-779d4a28ba5
    global minimal_word_length

    search_terms_id = terms[0]
    plain_search_term = terms[1]
    stemmed_search_term = terms[2]
    lemmatized_search_term = terms[3]

    record_counter = 1
    t = datetime.now()
    number_of_work_items = len(records)
    for record in records:
        if record_counter % 10000 == 0:
            ts = datetime.now() - start_time
            ts = ts.total_seconds()
            todo = (number_of_work_items - record_counter) * (ts / record_counter)
            print('\n----------------')
            print('Search counter : ', '{:,.0f}'.format(search_term_counter))
            print('Record counter : ', '{:,.0f}'.format(record_counter))
            print('Search term    : ', plain_search_term)
            print('Duration      : ', start_time.now() - start_time, ';', start_time.now())
        t = datetime.now()

        record_counter += 1
        repos_meta_id = str(record[0])
        repos_id = record[1]
        comment = record[2]
        sanitized_words = re.sub("[^a-zA-Z]", " ", comment).split()
        for w in sanitized_words:
            if len(w) >= minimal_word_length:
                # Plain
                if w == plain_search_term:
                    check_if_key_exist(repos_meta_id, search_terms_id, repos_id)
                    results[str(repos_meta_id)]['k' + str(search_terms_id)]['patterns_found'] += 1

                # Stemmed
                sw = stemmer.stem(w)
                if stemmed_search_term == sw:
                    check_if_key_exist(repos_meta_id, search_terms_id, repos_id)
                    results[str(repos_meta_id)]['k' + str(search_terms_id)]['stemmed_found'] += 1

                # Lemmatized
                lw = wordnet_lemmatizer.lemmatize(w)
                if lemmatized_search_term == lw:
                    check_if_key_exist(repos_meta_id, search_terms_id, repos_id)
                    results[str(repos_meta_id)]['k' + str(search_terms_id)]['lemmatized_found'] += 1


def process_records(search_terms, records):
    print('Processing records')

    search_term_counter = 0
    for search_term in search_terms:
        print ('>>>>> search_term: ', search_term)
        search_terms_id = str(search_term[0])

        # search_pattern = str(search_term[1], 'utf-8')
        # search_stemmed = str(search_term[2], 'utf-8')
        # search_lemmatized = str(search_term[3], 'utf-8')

        search_pattern = str(search_term[1])
        search_stemmed = str(search_term[2])
        search_lemmatized = str(search_term[3])
        terms = [search_terms_id, search_pattern, search_stemmed, search_lemmatized]
        search_terms_in_records(search_term_counter, terms, records)
        search_term_counter += 1


def save_results():
    filename = ANALYSIS_DIR + '/' + 'search_results.csv'
    print('>>>>> Writing out_file to: ',  filename)
    f = open(filename, 'w')
    # create the csv writer
    writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    row = ['repos_id', 'repos_meta_id', 'search_terms_id', 'patterns_found', 'stemmed_found', 'lemmatized_found']
    # write a row to the csv file
    writer.writerow(row)

    for repos_meta_id in results:
        # print(results[repos_meta_id])
        keys = results[repos_meta_id].keys()
        repos_id = results[repos_meta_id]['repos_id']
        # print(repos_id, repos_meta_id)
        # print ('keys', keys)
        for key in keys:
            if key != 'repos_id':
                search_terms_id = key.replace('k', '')
                # print ('1????? key=', key, ',', results[str(repos_meta_id)]['repos_id'])
                # print ('2????? key=', key, ',', type(results[str(repos_meta_id)][key]))
                # print ('3????? key=', key, ',', type(results[str(repos_meta_id)][key]), ',', results[str(repos_meta_id)][key])
                patterns_found = results[str(repos_meta_id)][key]['patterns_found']
                stemmed_found = results[str(repos_meta_id)][key]['stemmed_found']
                lemmatized_found = results[str(repos_meta_id)][key]['lemmatized_found']
                # print(repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found)

                row = [repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found]

                # write a row to the csv file
                writer.writerow(row)
    f.close()

def write_records_to_database(mycursor, records_to_insert):
    query = 'insert into search_results (repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found) values (%s, %s, %s, %s, %s, %s)'
    print ('                          Query : ', query)
    try:
       mycursor.executemany(query, records_to_insert)
       records_to_insert = []
    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))
        print('!!!')
        print('!     query : ', query)
        print('! len(row)  : ', len(records_to_insert))
        print('! first row : ', records_to_insert[0])
        sys.exit()
    return []

def save_results_to_database():
    print('\n----------------')
    print('Saving results with # of results : ', len(results))

    line_counter = 0
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    records_to_insert = []
    for repos_meta_id in results:
        keys = results[repos_meta_id].keys()
        repos_id = results[repos_meta_id]['repos_id']

        if line_counter == 0:
            query = 'delete from search_results where repos_id=%s'
            data = (repos_id,)
            print('Delete query: ', query)
            print('Delete query: ', data)
            mycursor.execute(query, data)


        for key in keys:
            if key != 'repos_id':
                search_terms_id = key.replace('k', '')
                patterns_found = results[str(repos_meta_id)][key]['patterns_found']
                stemmed_found = results[str(repos_meta_id)][key]['stemmed_found']
                lemmatized_found = results[str(repos_meta_id)][key]['lemmatized_found']
                # print(repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found)

                row = (repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found)

                records_to_insert.append(row)
                print('±±±±± records_to_insert', len(records_to_insert))
                print('±±±±± len(row)', len(row))

                if len(records_to_insert) > 1000:
                    records_to_insert = write_records_to_database(mycursor, records_to_insert)
        line_counter +=1

    write_records_to_database(mycursor, records_to_insert)

    sys.exit()

records = get_repo_rows()
search_terms = get_search_terms()
print ('>>>>> search terms: ', search_terms)
process_records(search_terms, records)
# save_results()
save_results_to_database()

print('Done: ', start_time.now() - start_time, start_time.now())
