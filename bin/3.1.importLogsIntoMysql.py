import os
import pandas as pd
import glob
import mysql.connector
import csv
import sys
from datetime import datetime
import argparse
import dbconfig as cfg

start_time = datetime.now()
t = datetime.now()

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')
args = parser.parse_args()
repo_name = args.repository

csv.field_size_limit(csv.field_size_limit() * 10)

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
csv.field_size_limit(sys.maxsize)

BASE_DIRECTORY = os.getcwd()
WORKING_DIRECTORY = BASE_DIRECTORY + '/../analysis/'
os.chdir(WORKING_DIRECTORY)

repos = [f.name for f in os.scandir() if f.is_dir() and f.name != 'analysis' and f.name != '.ipynb_checkpoints']
repos.sort()

# repos = ['WordPress', 'angular', 'ansible', 'atom', 'azure-docs', 'bash', 'bitcoin', 'brew', 'cassandra', 'che',
# 'cpython', 'express', 'flutter', 'git', 'jenkins', 'kubernetes', 'latex2e', 'linux', 'mediawiki', 'nginx',
# 'notebook', 'rails', 'react-native', 'tensorflow', 'vscode']


repos = [repo_name]


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password'
        )
    return mydb


def get_repos_id(mydb, name):
    mycursor = mydb.cursor()
    query = 'select id from repos where name=%s'
    data = (name,)
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    repos_id = myresult[0][0]
    mycursor.close()
    return repos_id


mydb = create_db_connection()

for d in repos:

    path = r'' + WORKING_DIRECTORY + d + '/'

    all_files = glob.glob(path + "/*.csv")

    repos_id = get_repos_id(mydb, d)
    print("\nRepository " + path)
    print('repos_id', repos_id)
    print('all_files: ', all_files)

    mycursor = mydb.cursor()
    mycursor.execute("SET NAMES 'utf8mb4'")
    mycursor.execute("SET CHARACTER SET utf8mb4")
    mycursor.execute('SET character_set_connection=utf8mb4')

    for filename in all_files:
        if 'messages.csv' in filename:
            print(">>>>> " + filename)
            df = pd.read_csv(filename, delimiter=';', quotechar='"', header=0, encoding_errors='ignore',
                             engine='python')  # ,  quoting=csv.QUOTE_NONE, , warn_bad_lines=True, error_bad_lines=False
            print(df.shape)
            number_of_work_items = df.shape[0]
            for index, row in df.iterrows():
                hashes = row[0] if row[0] == row[0] else ''
                parentHashes = row[1] if row[1] == row[1] else ''
                author = row[2] if row[2] == row[2] else ''
                authorDate = row[3] if row[3] == row[3] else ''
                comitter = row[4] if row[4] == row[4] else ''
                comitterDate = row[5] if row[5] == row[5] else ''
                subject = row[6] if row[6] == row[6] else ''
                comment = row[7] if row[7] == row[7] else ''

                authorDate = datetime.strptime(authorDate, '%a %b %d %H:%M:%S %Y')
                comitterDate = datetime.strptime(comitterDate, '%a %b %d %H:%M:%S %Y')

                # 
                query = 'select id from repos_meta where repos_id=%s and hash=%s'
                data = (repos_id, hashes,)
                mycursor.execute(query, data)
                myresult = mycursor.fetchall()
                count = mycursor.rowcount
                meta_repos_id = 0

                if count == 0:
                    query = "insert into repos_meta (repos_id, hash, parentHashes, author, authorDate, comitter, comitterDate, subject, comment) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                    data = (
                        repos_id, hashes, parentHashes, author, authorDate, comitter, comitterDate, subject, comment,)
                else:
                    meta_repos_id = myresult[0][0]
                    query = "update repos_meta set "
                    query += "repos_id=%s,"
                    query += "hash=%s,"
                    query += "parentHashes=%s,"
                    query += "author=%s,"
                    query += "authorDate=%s,"
                    query += "comitter=%s,"
                    query += "comitterDate=%s,"
                    query += "subject=%s,"
                    query += "comment=%s,"
                    query += "modification_date=now(),"
                    query += "attempts=attempts+1 "
                    query += "where id=%s"
                    data = (
                        repos_id, hashes, parentHashes, author, authorDate, comitter, comitterDate, subject, comment,
                        meta_repos_id,)

                if index > 0 and index % 1000 == 0:
                    ts = datetime.now() - start_time
                    ts = ts.total_seconds()
                    todo = (number_of_work_items - index) * (ts / index)
                    print('\n-----------------')
                    print(index, datetime.now() - t, datetime.now() - start_time, '; seconds=', todo,
                          '; minutes=', round(todo / 60, 2), '; hours=', round(todo / 3600, 2), '; day =',
                          round(todo / 3600 / 24, 2))
                    print('>>>>> query: ', query)
                    t = datetime.now()

                try:
                    mycursor.execute(query, data)
                    mydb.commit()
                    meta_repos_id = mycursor.lastrowid
                except mysql.connector.Error as err:
                    print("Something went wrong: {}".format(err))
                    print('meta_repos_id: ', meta_repos_id, query, data)

    mycursor.close()

mydb.close()

