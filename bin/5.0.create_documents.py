import pandas as pd
from datetime import datetime
import numpy as np
import mysql.connector
import sys
import csv
import os
import argparse

start_time = datetime.now()

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')
args = parser.parse_args()
repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR = '/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print('ANALYSIS_DIR :', ANALYSIS_DIR)


def unique(list1):
    x = np.array(list1)
    return (np.unique(x))


def create_db_connection():
    mydb = mysql.connector.connect(
        host="node157.tezzt.nl",
        user="theotheu",
        password='aQ743Gp-',
        database='new_approaches',
    )
    return mydb

def run_query_in_parts(mycursor, hashes):
    chunk_size=100
    l = len(hashes)
    l_counter = 0
    joined_results = []
    while l_counter<l:
        some_hashes = hashes[l_counter:l_counter + chunk_size]

        query = 'select UNIX_TIMESTAMP(comitterDate) comitterDate, comment from repos_meta where hash in ("'
        query += '","'.join(some_hashes)
        query += '") '
        # query += 'order by comitterDate'
        data = ()
        # print('>>>>> query: ', query)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        joined_results = [*joined_results, *myresult]


        l_counter +=chunk_size


    joined_results.sort(key=lambda tup: tup[0])

    result = [a_tuple[1] for a_tuple in joined_results]

    comments = ' '.join(result)
    comments = comments.replace('\n', ' ')

    return comments



def read_file_from_disk(dir, in_filename, out_filename):
    mydb = create_db_connection()
    mycursor = mydb.cursor()

    hashes_with_branches = {}
    branches_with_hashes = {}

    filename = dir + '/' + in_filename

    print ('>>>>> Start  reading ' + filename)
    df = pd.read_csv(filename, delimiter=';', quotechar='"', header=0,
                     engine='python', encoding_errors='ignore')  # , header=True, index_col=0, squeeze=True)
    print('>>>>> Done  reading ', datetime.now()-start_time)

    print(df.shape)
    number_of_work_items = df.shape[0]

    t = datetime.now()

    for index, row in df.iterrows():
        hash = row[0]
        if type(row[1]) == str:
            if index % 10000 == 0:
                ts = datetime.now() - start_time
                ts = ts.total_seconds()
                if ts != 0 and index != 0:
                    todo = (number_of_work_items - index) * (ts / index)
                    print('another batch: ', '{:,.2f}'.format(index, '/', df.shape[0]), datetime.now() - start_time)
                    print(dir, index, datetime.now() - t, datetime.now() - start_time, '; seconds=', round(todo),
                          '; minutes=', round(todo / 60, 2), '; hours=', round(todo / 3600, 2), '; day =',
                          round(todo / 3600 / 24, 2))
                t = datetime.now()

            branches = row[1].split('<<<SEP>>>')
            hashes_with_branches[hash] = branches

            for branch in branches:
                if branch not in branches_with_hashes:
                    branches_with_hashes[branch] = []
                # if hash not in branches_with_hashes[branch]:
                branches_with_hashes[branch].append(hash)

    # print(len(hashes_with_branches))
    # print(hashes_with_branches['3642c401b67e051a6a7a31db627b8099e42318d9'])
    print(len(branches_with_hashes))
    filename = dir + '/' + out_filename
    print('>>>>> Writing out_file to: ', filename)
    f = open(filename, 'w')
    # create the csv writer
    writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

    row = ['Branch', 'Document']
    # write a row to the csv file
    writer.writerow(row)
    f.close()

    branch_counter = 1
    for branch in branches_with_hashes:
        l = unique(branches_with_hashes[branch])
        print('\n------------------------------------------')
        print('direcytory         : ', branch)
        print('comments in branch : ', '{:,.0f}'.format(len(l)))
        print('branch counter     : ', branch_counter)
        print('total branches     : ', len(branches_with_hashes))
        print('duration           : ', datetime.now() - start_time)
        branch_counter += 1

        comments = run_query_in_parts(mycursor, l)

        row = [branch, comments]

        f = open(filename, 'a')
        # create the csv writer
        writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

        # write a row to the csv file
        writer.writerow(row)

        # close the file
        f.close()
    mycursor.close()
    mydb.close()

dir = ANALYSIS_DIR
in_filename = 'hashes_with_branches.csv'
out_filename = 'documents.csv'

read_file_from_disk(dir, in_filename, out_filename)
