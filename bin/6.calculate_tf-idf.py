import sys
from datetime import datetime
from csv import reader
import re
import nltk
import csv
import sys
import math
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
import numpy as np
import hashlib
import os
import argparse
nltk.download('wordnet')
nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
csv.field_size_limit(sys.maxsize)
start_time = datetime.now()

Nt_plain_dict = {}
Nt_stemmed_dict = {}
FT_plain_dict = {}
FT_stemmed_dict = {}
number_of_documents = 0
plain_words_per_document = {}

start_time = datetime.now()
minimal_word_length = 2

in_filename = 'documents.csv'

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')

args = parser.parse_args()

repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR='/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print ('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print ('ANALYSIS_DIR :', ANALYSIS_DIR)



def unique(list1):
    x = np.array(list1)
    return (np.unique(x))


def cleaner_stemmer_lemmatizer(key, field):
    # https://towardsdatascience.com/multi-class-text-classification-with-sklearn-and-nltk-in-python-a-soFT_ware-engineering-use-case-779d4a28ba5

    global minimal_word_length

    field = field.lower()
    words_only = []
    stemmed = []
    lemmatized = []

    sanitized_words = re.sub("[^a-zA-Z]", " ", field).split()
    word_counter = 0

    for w in sanitized_words:
        sw = stemmer.stem(w)
        lw = wordnet_lemmatizer.lemmatize(w)
        words_only.append(w)
        stemmed.append(sw)
        lemmatized.append(lw)

        if w not in Nt_plain_dict:
            Nt_plain_dict[w] = 0
        if w not in FT_plain_dict[key]:
            FT_plain_dict[key][w] = 0
        if sw not in Nt_stemmed_dict:
            Nt_stemmed_dict[sw] = 0
        if sw not in FT_stemmed_dict[key]:
            FT_stemmed_dict[key][sw] = 0

        if len(w) >= minimal_word_length:
            word_counter += 1
            Nt_plain_dict[w] += 1
            Nt_stemmed_dict[sw] += 1
            FT_plain_dict[key][w] += 1
            FT_stemmed_dict[key][sw] += 1

    unique_words_only = unique(words_only)
    unique_stemmed = unique(stemmed)
    unique_lemmatized = unique(lemmatized)

    retVal = [words_only, stemmed, lemmatized, len(unique_words_only), len(unique_stemmed), len(unique_lemmatized),
              word_counter]

    return retVal


def read_file_from_disk(dir, in_filename):
    global number_of_documents

    document_hashes = []

    filename = dir + '/' + in_filename

    print('>>>>> Reading from : ', filename)

    # open file in read mode
    with open(filename, 'r') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj, delimiter=';', quotechar='"')
        # Iterate over each row in the csv using reader object
        row_counter = 0
        for row in csv_reader:
            if row_counter > 0:
                branch = row[0]
                document = row[1]
                md5 = hashlib.md5(document.encode()).hexdigest()

                if md5 not in document_hashes:
                    document_hashes.append(md5)

                    FT_plain_dict[branch] = {}
                    FT_stemmed_dict[branch] = {}

                    # row variable is a list that represents a row in csv
                    r = cleaner_stemmer_lemmatizer(branch, document)
                    plain_words_per_document[branch] = r[3]
                    print('\n----- reading from directory            : ', filename)
                    print('----- branch                            : ', branch)
                    print('----- number of documents               : ', '{:,.0f}'.format(number_of_documents))
                    print('----- number of plain words             : ', '{:,.0f}'.format(r[3]))
                    print('----- number of stemmed words           : ', '{:,.0f}'.format(r[4]))
                    print('----- number of lemmatized words        : ', '{:,.0f}'.format(r[5]))
                    print('----- total number of words for document: ', '{:,.0f}'.format(r[6]))
                    print('----- duration: ', start_time.now() - start_time, start_time.now())
                    number_of_documents += 1
            row_counter += 1


read_file_from_disk(ANALYSIS_DIR, in_filename)

print('\n>>>>> number of documents: ', '{:,.0f}'.format(number_of_documents))
print('>>>>> unique plain words for all documents: ', '{:,.0f}'.format(len(Nt_plain_dict)))
print('>>>>> unique stemmed words for all documents: ', '{:,.0f}'.format(len(Nt_stemmed_dict)))
print('>>>>> duration: ', start_time.now() - start_time, start_time.now())


def save_tfidf():
    # #########################
    filename = ANALYSIS_DIR + '/tf-idf.csv'
    f = open(filename, 'w')
    # create the csv writer
    writer = csv.writer(f, delimiter=';', quotechar='"') #, quoting=csv.QUOTE_ALL

    # Write header
    # https://dataaspirant.com/tf-idf-term-frequency-inverse-document-frequency/
    row = ['Branch', 'Word', 'Σf(t,d)', 'Term-Frequency (TF)', 'Nt', 'N/Nt', 'IDF', 'TF-IDF']
    # write a row to the csv file
    writer.writerow(row)

    word_counter = 1
    for w in Nt_plain_dict:
        if word_counter % 1000 == 0:
            print('\n------------------------')
            print('----- Writing to   : ', filename)
            print('----- #            : ', word_counter)
            print('----- total words  : ', '{:,.0f}'.format(len(Nt_plain_dict)))
            print('----- current word : ', w)
            print('----- duration     : ', start_time.now() - start_time, start_time.now())

        word_counter += 1

        Nt = Nt_plain_dict[w]
        # print('Nt      : ', '{:,.0f}'.format(Nt))

        for b in FT_plain_dict:
            # print ('d ......: ', b)
            all_words_per_document = plain_words_per_document[b]
            # print('Σf      : ', '{:,.0f}'.format(all_words_per_document))
            # for w in FT_plain_dict[b]:
            if w in FT_plain_dict[b]:
                unique_words = FT_plain_dict[b][w]
            else:
                unique_words = 0
            # print('tf(i,d) : ', '{:,.0f}'.format(unique_words))

            tf = unique_words / all_words_per_document
            # print ('tf      : ', '{:,.2f}'.format(tf))
            try:
                nnt = number_of_documents / (1 + Nt)
            except:
                print('ERROR: ', w, Nt)
                sys.exit()
            idf = math.log10(nnt)
            # print ('idf     : ', '{:,.2f}'.format(idf))

            tfidf = tf * idf
            # print ('tf-idf  : ', '{:,.2f}'.format(tfidf))

            row = [b, w, all_words_per_document, str(tf), str(Nt), str(nnt), str(idf), str(tfidf)]
            writer.writerow(row)

    # close the file
    f.close()


save_tfidf()
